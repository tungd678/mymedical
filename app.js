const express = require('express');
const hbs = require('express-handlebars');
const bodyParser = require('body-parser');
const session = require('express-session');
const passport = require('passport');
const methodOverride = require('method-override');
const fileUpload = require('express-fileupload');
const paginate = require('express-handlebars-paginate');
const flash = require('connect-flash');
const configPassport = require('./config/passport');
const models = require('./models');
const routes = require('./routes');
const app = express();

// Set static folder
app.use(express.static(__dirname + '/public'));
app.use('/css', express.static(__dirname + '/node_modules/bootstrap/dist/css'));
app.use('/css', express.static(__dirname + '/node_modules/font-awesome/css'));
app.use('/js', express.static(__dirname + '/node_modules/bootstrap/dist/js'));
app.use('/js', express.static(__dirname + '/node_modules/jquery/dist'));
app.use('/js', express.static(__dirname + '/node_modules/popper.js/dist'));

// Set up method override
app.use(methodOverride('_method'));

// Set up file upload
app.use(fileUpload());

// Set up flash notification
app.use(flash());

// Set up view engine
app.engine('hbs', hbs({
    extname: 'hbs',
    defaultLayout: 'layout',
    layoutsDir: __dirname + '/views/layouts',
    partialsDir: __dirname + '/views/partials',
    helpers: {
        paginate: paginate.createPagination,
        eq(v1, v2) {
            return v1 === v2;
        },
        ne(v1, v2) {
            return v1 !== v2;
        },
        lt(v1, v2) {
            return v1 < v2;
        },
        gt(v1, v2) {
            return v1 > v2;
        },
        lte(v1, v2) {
            return v1 <= v2;
        },
        gte(v1, v2) {
            return v1 >= v2;
        },
        and() {
            return Array.prototype.slice.call(arguments).every(Boolean);
        },
        or() {
            return Array.prototype.slice.call(arguments, 0, -1).some(Boolean);
        },
        formatDate(str) {
            return new Date(str).toISOString().slice(0, 10);
        },
        getTime(str) {
            return new Date(str).toLocaleTimeString();
        }
    }
}));
app.set('view engine', 'hbs');

// Set up body parser
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// Set up passport
app.use(session({
    secret: 'asdlkjsdajf',
    resave: true,
    saveUninitialized: true
})); // session secret
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions

configPassport(passport);

// Sync table
app.get('/sync', (req, res) => {
    models.sequelize
        .sync()
        .then(() => {
            res.send('tables created!');
        });
});

// Middleware to pass current user
app.use((req, res, next) => {
    res.locals.currentUser = req.user;
    res.locals.error = req.flash('error');
    res.locals.success = req.flash('success');
    next();
});

// Routing
app.use('/', routes);

// Listening
const port = process.env.PORT || 8080;
app.listen(port, () => {
    console.log(`Server is up at port ${port}`);
});