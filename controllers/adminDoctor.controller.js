const { User, ChuyenKhoa, LamViecTai, PhongKham } = require("../models");
const sequelize = require("sequelize");

module.exports.getDoctorInfo = async (req, res) => {
  try {
    let bacSi = await User.findAll({
      attributes: [
        "id",
        "HoTen",
        "SoDienThoai",
        "Email",
        [
          sequelize.fn(
            "to_char",
            sequelize.col("User.createdAt"),
            "DD Mon YYYY"
          ),
          "createdAt"
        ],
        "TrangThai"
      ],
      include: [
        {
          model: ChuyenKhoa,
          attributes: ["Ten"],
          required: true
        },
        {
          model: PhongKham,
          as: "PhongKham",
          attributes: ["Ten"]
        }
      ],
      where: { LoaiUser: 2 }
    });

    const limit = 7;
    const page = parseInt(req.query.page) || 1;
    const offset = (page - 1) * limit;

    const pagination = {
      page,
      limit,
      totalRows: bacSi.length
    };
    let bacsi = bacSi.slice(offset, offset + limit);

    res.render("adminDoctor", { layout: "adminLayout.hbs", bacsi, pagination });
  } catch (error) {
    console.log(error);
  }
};

module.exports.updateTrangThai = async (req, res) => {
  try {
    const id = req.params.bacsiId;
    var TrangThai = parseInt(req.body.action);

    const bacSi = await User.findOne({ where: { id } });
    bacSi.update({ TrangThai });

    res.redirect("back");
  } catch (error) {
    console.log(error);
  }
};

module.exports.xoaBacSi = async (req, res) => {
  try {
    const id = req.params.bacsiId;
    User.destroy({ where: { id } });
    res.redirect("back");
  } catch (error) {
    console.log(error);
  }
};
