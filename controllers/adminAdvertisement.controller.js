const { QuangCao } = require("../models");
const sequelize = require("sequelize");

module.exports.getAdvertisement = async (req, res) => {
  try {
    const quangCao = await QuangCao.findAll({
      attributes: [
        "id",
        "Ten",
        "HinhAnh",
        "TrangThai",
        [
          sequelize.fn(
            "to_char",
            sequelize.col("ThoiGianBatDau"),
            "DD Mon YYYY"
          ),
          "ThoiGianBatDau"
        ],
        [
          sequelize.fn(
            "to_char",
            sequelize.col("ThoiGianKetThuc"),
            "DD Mon YYYY"
          ),
          "ThoiGianKetThuc"
        ],
        "Loai"
      ]
    });

    const limit = 7;
    const page = parseInt(req.query.page) || 1;
    const offset = (page - 1) * limit;

    const pagination = {
      page,
      limit,
      totalRows: quangCao.length
    };
    let quangcao = quangCao.slice(offset, offset + limit);

    res.render("adminAdvertisement", {
      layout: "adminLayout.hbs",
      quangcao,
      pagination
    });
  } catch (error) {
    console.log(error);
  }
};

module.exports.xoaAd = async (req, res) => {
  try {
    const id = req.params.adId;
    QuangCao.destroy({ where: { id } });
    res.redirect("back");
  } catch (error) {
    console.log(error);
  }
};
