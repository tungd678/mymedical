const { PhongKham, User, ChuyenKhoa, GomCacKhoa } = require("../models");
const sequelize = require("sequelize");

module.exports.getCategory = async (req, res) => {
  try {
    let chuyenKhoa = await ChuyenKhoa.findAll({
      attributes: [
        "id",
        "Ten",
        [sequelize.fn("count", "GomCacKhoa.ChuyenKhoaId"), "SLPhongKham"]
      ],
      include: [
        {
          model: GomCacKhoa,
          attributes: []
        }
      ],
      group: ["ChuyenKhoa.id"],
      order: [["id", "ASC"]]
    });
    chuyenKhoa = chuyenKhoa.map(i => i.dataValues);

    let soLuongBS = await ChuyenKhoa.findAll({
      attributes: [
        "id",
        "Ten",
        [sequelize.fn("count", "User.ChuyenKhoaId"), "SLBacSi"]
      ],
      include: [
        {
          model: User,
          attributes: [],
          where: { LoaiUser: 2 }
        }
      ],
      group: ["ChuyenKhoa.id"],
      order: [["id", "ASC"]]
    });

    soLuongBS = soLuongBS.map(i => i.dataValues);
    for (var i = 0; i < chuyenKhoa.length; i++) {
      Object.assign(chuyenKhoa[i], soLuongBS[i]);
    }

    const limit = 7;
    const page = parseInt(req.query.page) || 1;
    const offset = (page - 1) * limit;

    const pagination = {
      page,
      limit,
      totalRows: chuyenKhoa.length
    };
    let chuyenkhoa = chuyenKhoa.slice(offset, offset + limit);

    res.render("adminCategory", {
      layout: "adminLayout.hbs",
      chuyenkhoa,
      pagination
    });
  } catch (error) {
    console.log(error);
  }
};

module.exports.xoaCategory = async (req, res) => {
  try {
    const id = req.params.chuyenkhoaId;
    ChuyenKhoa.destroy({ where: { id } });
    res.redirect("back");
  } catch (error) {
    console.log(error);
  }
};

module.exports.searchCategory = async (req, res) => {
  try {
    let name = req.query.tenKhoa.toLowerCase();
    
    let chuyenKhoa = await ChuyenKhoa.findAll({
      attributes: [
        "id",
        "Ten",
        [sequelize.fn("count", "GomCacKhoa.ChuyenKhoaId"), "SLPhongKham"]
      ],
      include: [
        {
          model: GomCacKhoa,
          attributes: []
        }
      ],
      where: {
        Ten: sequelize.where(
          sequelize.fn("LOWER", sequelize.col("Ten")),
          "LIKE",
          "%" + name + "%"
        )
      },
      group: ["ChuyenKhoa.id"],
      order: [["id", "ASC"]]
    });
    chuyenKhoa = chuyenKhoa.map(i => i.dataValues);

    let soLuongBS = await ChuyenKhoa.findAll({
      attributes: [
        "id",
        "Ten",
        [sequelize.fn("count", "User.ChuyenKhoaId"), "SLBacSi"]
      ],
      include: [
        {
          model: User,
          attributes: [],
          where: { LoaiUser: 2 }
        }
      ],
      group: ["ChuyenKhoa.id"],
      order: [["id", "ASC"]]
    });

    soLuongBS = soLuongBS.map(i => i.dataValues);
    for (var i = 0; i < chuyenKhoa.length; i++) {
      for (var j = 0; j < soLuongBS.length; j++) {
        if (chuyenKhoa[i].id == soLuongBS[j].id) {
          Object.assign(chuyenKhoa[i], soLuongBS[j]);
        }
      }
    }

    const limit = 7;
    const page = parseInt(req.query.page) || 1;
    const offset = (page - 1) * limit;

    const pagination = {
      page,
      limit,
      totalRows: chuyenKhoa.length
    };
    let chuyenkhoa = chuyenKhoa.slice(offset, offset + limit);

    res.render("adminCategory", {
      layout: "adminLayout.hbs",
      chuyenkhoa,
      pagination
    });
  } catch (error) {
    console.log(error);
  }
};
