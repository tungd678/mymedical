const { User, PhongKham, QuangCao, ChuyenKhoa, TinhThanhPho } = require('../models');

function getHomePage(req, res) {
    Promise.all([
        PhongKham.findAll({
            attributes: ['id', 'Ten', 'Thumbnail'],
            limit: 6
        }),
        User.findAll({
            attributes: ['id', 'HoTen', 'Avatar', 'BangCap'],
            limit: 3,
            where: {
                LoaiUser: 2
            },
            include: [
                {
                    model: ChuyenKhoa,
                    attributes: ['Ten'],
                }
            ]
        }),
        QuangCao.findAll({
            where: {
                TrangThai: true
            },
            attributes: ['id', 'HinhAnh']
        }),
        TinhThanhPho.findAll({ attributes: ['id', 'name'] }),
        ChuyenKhoa.findAll()
    ])
        .then(result => {
            res.render('index', {
                PhongKhams: result[0],
                BacSis: result[1],
                QuangCaos: result[2],
                tinhThanhPho: result[3],
                chuyenKhoa: result[4]
            })
        })
        .catch(err => {
            console.log(err);
        })
}

module.exports = {
    getHomePage
}