const passport = require('passport');

function logout(req, res) {
    req.logout();
    req.flash('success', 'Bạn đã đăng xuất thành công!');
    req.session.save((err) => {
        if (err) {
            console.log(err);
        } else {
            // req.flash('success', 'Bạn đã đăng xuất thành công!');
            res.redirect('/');
        }
    });
}

module.exports = {
    logout
}