const {
    User,
    ThanhToan,
    LichHen,
    ChiTietThueDichVu,
} = require('../models');
const sequelize = require('sequelize');
var A = Date.now();

async function getDashboard(req, res) {
    try {
        let thongKe = await User.findAll({
            attributes:[[sequelize.fn('count', 'id'), 'NguoiDung']]
        });
        thongKe = thongKe.map(i=>i.dataValues);

        let donHen = await LichHen.findAll({
            attributes:[[sequelize.fn('count', 'id'), 'DonHen']],
            where:{
                TrangThai: 2
            }
        });
        donHen = donHen.map(i=>i.dataValues);

        let doanhThu = [];
        await ThanhToan.sum(
            'TongGiaTri',
            {
                include:[{
                    model: ChiTietThueDichVu,
                    attributes: [],
                    where: {
                        GoiDichVuId: 1
                    }
                }]
            }
        ).then(sum=>{
            doanhThu.push(sum);
        })

        await ThanhToan.sum(
            'TongGiaTri',
            {
                include:[{
                    model: ChiTietThueDichVu,
                    attributes: [],
                    where: {
                        GoiDichVuId: 2
                    }
                }]
            }
        ).then(sum=>{
            doanhThu.push(sum);
        })

        Object.assign(thongKe[0], donHen[0]);
        doanhThu = JSON.stringify(doanhThu);

        res.render('adminDashboard', {layout: 'adminLayout.hbs', thongKe, doanhThu});
    } catch (error) {
        console.log(error);
    }
}

module.exports = {
    getDashboard
}