const { User, LichHen, PhongKham } = require('../models');

async function getAppointmentSchedulePage(req, res) {
    try {
        const id = req.params.id;

        const nguoiDung = await User.findOne({ 
            where: { id }
        });

        const lichHen = await LichHen.findAll({
            where: {
                UserId: id,
            },
            include: [
                {
                    model: PhongKham,
                    attributes: ['Ten']
                }
            ]
        });

        res.render('User-AppointmentSchedule', { nguoiDung, lichHen });
    } catch (error) {
        console.log(error);
        req.flash('error', 'Lỗi!')
        res.redirect('back');
    }
}

module.exports = {
    getAppointmentSchedulePage
}