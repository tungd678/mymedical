const { User } = require("../models");
const sequelize = require("sequelize");

module.exports.getUserInfo = async (req, res) => {
  try {
    const nguoiDung = await User.findAll({
      attributes: [
        "id",
        "Username",
        "HoTen",
        "SoDienThoai",
        "Email",
        [
          sequelize.fn("to_char", sequelize.col("createdAt"), "DD Mon YYYY"),
          "createdAt"
        ],
        "TrangThai"
      ]
    });

    const limit = 7;
    const page = parseInt(req.query.page) || 1;
    const offset = (page - 1) * limit;

    const pagination = {
      page,
      limit,
      totalRows: nguoiDung.length
    };
    let nguoidung = nguoiDung.slice(offset, offset + limit);

    res.render("adminUser", {
      layout: "adminLayout.hbs",
      nguoidung,
      pagination
    });
  } catch (error) {
    console.log(error);
  }
};

module.exports.updateTrangThai = async (req, res) => {
  try {
    const id = req.params.userId;
    var TrangThai = parseInt(req.body.action);

    const nguoiDung = await User.findOne({ where: { id } });
    nguoiDung.update({ TrangThai });

    res.redirect("back");
  } catch (error) {
    console.log(error);
  }
};

module.exports.xoaUser = async (req, res) => {
  try {
    const id = req.params.userId;

    User.destroy({ where: { id } });
    res.redirect("back");
  } catch (error) {
    console.log(error);
  }
};