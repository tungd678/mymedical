const { ChuyenKhoa } = require("../models");

module.exports.getAddCategory = async (req, res) => {
  try {
    res.render("adminAddCategory", { layout: "adminLayout.hbs" });
  } catch (error) {
    console.log(error);
  }
};

module.exports.addCategory = async (req, res) => {
  try {
    const result = await ChuyenKhoa.create({
      Ten: req.body.tenChuyenKhoa,
      MoTa: req.body.moTa
    });

    res.redirect("back");
  } catch (error) {
    console.log(error);
  }
};
