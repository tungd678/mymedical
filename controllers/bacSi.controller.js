const { User, ChuyenKhoa, LamViecTai, PhongKham, ChiTietDiaChi, QuanHuyen, GomCacKhoa } = require('../models');
const sequelize = require('sequelize');
const Op = sequelize.Op;
const showdown = require('showdown');
const converter = new showdown.Converter();

module.exports.getBacSiPage = async (req, res) => {
    try {
        const id = req.params.id;

        const bacSi = await User.findOne({
            where: {
                id,
                LoaiUser: 2
            },
            include: [
                {
                    model: ChuyenKhoa,
                    attributes: ['Ten']
                }
            ]
        });

        let phongKhamId = await LamViecTai.findAll({
            where: {
                BacSiId: id,
                TrangThai: true
            },
            attributes: ['PhongKhamId']
        });

        phongKhamId = phongKhamId.map(i => i.dataValues.PhongKhamId);

        const phongKham = await PhongKham.findAll({
            attributes: ['id', 'Ten', 'Thumbnail'],
            where: {
                id: {
                    [Op.in]: phongKhamId
                },
            },
            include: [
                {
                    model: ChiTietDiaChi,
                    attributes: ['QuanHuyenId', 'Duong'],
                    include: [
                        {
                            model: QuanHuyen,
                            attributes: ['path_with_type']
                        }
                    ]
                }
            ]
        });

        if (bacSi) {
            bacSi.ThongTinChiTiet = converter.makeHtml(bacSi.ThongTinChiTiet);
            res.render('bacSi', {
                bacSi,
                phongKham
            });
        } else {
            res.redirect('back');
        }
    } catch (error) {
        console.log(error);
        res.redirect('back');
    }
}

module.exports.getQuanLyBacSi = async (req, res) => {
    try {
        const id = req.params.id;

        const bacSi = await User.findOne({
            where: {
                id,
                LoaiUser: 2
            },
            include: [
                {
                    model: ChuyenKhoa,
                    attributes: ['id', 'Ten']
                }
            ]
        });

        let phongKhamId = await LamViecTai.findAll({
            where: {
                BacSiId: id,
                TrangThai: true
            },
            attributes: ['PhongKhamId']
        });

        phongKhamId = phongKhamId.map(i => i.dataValues.PhongKhamId);

        const phongKham = await PhongKham.findAll({
            attributes: ['id', 'Ten', 'Thumbnail'],
            where: {
                id: {
                    [Op.in]: phongKhamId
                },
            },
            include: [
                {
                    model: ChiTietDiaChi,
                    attributes: ['QuanHuyenId', 'Duong'],
                    include: [
                        {
                            model: QuanHuyen,
                            attributes: ['path_with_type']
                        }
                    ]
                }
            ]
        });

        const chuyenKhoa = await ChuyenKhoa.findAll();

        if (bacSi) {
            res.render('quanLyBacSi', {
                bacSi,
                phongKham,
                chuyenKhoa
            });
        } else {
            res.redirect('back');
        }
    } catch (error) {
        console.log(error);
    }
}

module.exports.capNhapThongTinBacSi = async (req, res) => {
    try {
        await User.update(
            req.body,
            { where: { id: req.params.id } },
        );

        res.redirect('back');
    } catch (error) {
        console.log(error);
        res.redirect('back');
    }
}