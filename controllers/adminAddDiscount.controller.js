const { KhuyenMai, PhongKham } = require("../models");
const sequelize = require("sequelize");

module.exports.getAddDiscountInfo = async (req, res) => {
  try {
    const phongKham = PhongKham.findAll({
      attributes: ["Ten"]
    });

    res.render("adminAddDiscount", { layout: "adminLayout.hbs", phongKham });
  } catch (error) {
    console.log(error);
  }
};

module.exports.addDiscountInfo = async (req, res) => {
  try {
    const result = await KhuyenMai.create({
      Ten: req.body.tenKhuyenMai,
      TrangThai: true,
      ThoiGianBatDau: `${req.body.dateStart}`,
      ThoiGianKetThuc: `${req.body.dateEnd}`,
      MucGiam: req.body.mucGiam,
      Code: req.body.code
    });

    res.redirect('back');
  } catch (error) {
    console.log(error);
  }
};
