const { User } = require('../models');

module.exports.getLoginPage = (req, res) => {
    res.render('login', {
        referer: req.headers.referer
    });
}

module.exports.auth = (req, res) => {
    try {
        req.flash('success', 'Bạn đã đăng nhập thành công!');

        if (req.user.LoaiUser === 1) {
            return res.redirect('/admin');
        }

        const referer = req.body.referer;
        if (referer) {
            res.redirect(referer);
        } else {
            res.redirect('/');
        }
    } catch (error) {
        console.log(error);
    }
}