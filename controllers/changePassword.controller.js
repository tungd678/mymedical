const { User } = require('../models');
const bcrypt = require('bcryptjs');

async function getChangePasswordPage(req, res) {
    try{
        const id = req.params.id;
        const nguoiDung = await User.findOne({
            where:{
                id,
            },
            attributes:['id', 'HoTen', 'Password', 'SoDienThoai','Email','GioiTinh','NgaySinh','Avatar']
        });
        if(nguoiDung){
            res.render('User-ChangePassword',{
                nguoiDung,
            });
        }else{
            res.redirect('back');
        }

    }
    catch(error){
        console.log(error);
    }
    // res.render('User-ChangePassword');
}

async function updatePassword(req, res){
    try{
        const id = req.params.id;
        const salt = bcrypt.genSaltSync(10);

        const user = await User.findOne({where:{id}});

        if (!bcrypt.compareSync(req.body.oldPass, user.Password)) {
            req.flash('error', 'Mật khẩu cũ không chính xác!');
            return res.redirect('back');
        }

        await user.update({Password: bcrypt.hashSync(req.body.Password, salt)});

        req.flash('success', 'Đổi mật khẩu thành công!');

        res.redirect('back');
	} catch(error){
		console.log(error);
		// res.redirect('back');
	}
}
async function checkPassword(req, res){
    try{

        await User.findOne({
            where:{id : req.params.id}
        })
        .then(function(){
            const pass = req.body.Password;
            if(!bcrypt.compareSync(pass, User.Password)){
                return done(null, false, {message: 'Incorrec Password'});
            }
        })
        .catch(function(error){
            res.json(error);
        })
        
    }
    catch{
        console.log(error);
		// res.redirect('back');
    }
}
module.exports = {
    getChangePasswordPage, updatePassword,checkPassword
}