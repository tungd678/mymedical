const { User, PhongKham, TinhThanhPho, ChuyenKhoa, ChiTietDiaChi, LamViecTai } = require('../models');
const bcrypt = require('bcryptjs');
const { Op } = require('sequelize');
const fs = require('fs');
const crypto = require('crypto-random-string');
const mailjet = require('node-mailjet').connect('d9bacf7683a71ad3037d1138e2eb79b6', 'c7ce7da13af79159d3a52f398bdf5c8a');
const verifyEmail = require('../views/html/verifyEmail');

async function getRegisterPage(req, res) {
    try {
        const result = await Promise.all([
            PhongKham.findAll(),
            TinhThanhPho.findAll({ attributes: ['id', 'name'] }),
            ChuyenKhoa.findAll()
        ])

        res.render('register', {
            phongKham: result[0],
            tinhThanhPho: result[1],
            chuyenKhoa: result[2]
        });
    } catch (error) {
        console.log(error);
        res.redirect('back')
    }
}

function sendVerificationEmail(token, email) {
    // const hostUrl = process.env.hostURL || 'http://localhost:8080';
    const hostUrl = 'https://mymedical-nhom08.herokuapp.com';
    const verifyUrl = `${hostUrl}/dang-ky/verification?token=${token}&email=${email}`;
    const content = verifyEmail(verifyUrl);

    const msg = {
        Messages: [
            {
                From: {
                    Email: 'mymedical-nhom08@hotmail.com',
                    Name: 'MyMedical'
                },
                To: [
                    {
                        Email: email,
                        Name: 'MyMedical'
                    }
                ],
                Subject: 'MyMedical - Verify your email!',
                TextPart: '',
                HTMLPart: content
            }
        ]
    }

    const request = mailjet
        .post('send', { 'version': 'v3.1' })
        .request(msg);

    return request
        .then(result => console.log(result.body))
        .catch(err => console.log(err));
}

async function dangKy(req, res) {
    try {
        const salt = bcrypt.genSaltSync(10);
        const token = crypto({ length: 16 });

        const defaults = {
            HoTen: req.body.HoTen,
            Email: req.body.Email,
            SoDienThoai: req.body.SoDienThoai,
            Username: req.body.Username,
            Password: bcrypt.hashSync(req.body.Password, salt),
            LoaiUser: parseInt(req.body.LoaiUser),
            Avatar: '/images/wallhaven-648512.jpg',
            TrangThai: false,
            token
        }

        if (defaults.LoaiUser === 2) {
            defaults.ChuyenKhoaId = req.body.ChuyenKhoa;
        }

        const result = await User.findOrCreate({
            where: {
                [Op.or]: [
                    { Username: defaults.Username },
                    { Email: defaults.Email }
                ]
            },
            defaults
        });


        if (!result[1]) {
            // flash msg
            req.flash('error', 'Username hoặc Email đã tồn tại!');

            return res.redirect('back');
        } else {
            if (defaults.LoaiUser === 2) {
                const lamViecTai = await LamViecTai.create({ PhongKhamId: req.body.PhongKhamId, BacSiId: result[0].id, TrangThai: true });
            } else if (defaults.LoaiUser === 4) {
                const phongKham = await PhongKham.create({ Ten: req.body.TenPhongKham });
                const chiTietDiaChi = await ChiTietDiaChi.create({
                    PhongKhamId: phongKham.id,
                    TinhThanhPhoId: parseInt(req.body.TinhThanhPhoId),
                    QuanHuyenId: parseInt(req.body.QuanHuyenId),
                    Duong: req.body.Duong
                });
                const dir = `public/images/phongKham/${phongKham.id}`;
                const galleryDir = `public/images/phongKham/${phongKham.id}/gallery`;
                fs.mkdirSync(dir);
                fs.mkdirSync(galleryDir);
                fs.writeFileSync(`${galleryDir}/.keep`);

                await Promise.all([
                    result[0].update({ PhongKhamId: phongKham.id }),
                    phongKham.update({ Gallery: `/images/phongKham/${phongKham.id}/gallery` })
                ])
            }
        }

        await sendVerificationEmail(token, defaults.Email);

        req.flash('success', 'Đăng ký thành công! Vui lòng xác thực email để đăng nhập');
        res.redirect('/dang-nhap');
    } catch (error) {
        console.log(error);
        req.flash('error', 'Không thể đăng ký!');
        res.redirect('back');
    }
}

async function verifyAccount(req, res) {
    try {
        const Email = req.query.email;
        const token = req.query.token;
        const user = await User.findOne({ where: { Email } });
        let msg = '';

        if (user.TrangThai) {
            msg = 'Tài đã được xác thực trước đó!';
        } else {
            if (token === user.token) {
                await user.update({ TrangThai: true });

                msg = 'Tài khoản đã được xác thực!';
            }
        }

        res.render('verification', { msg });
    } catch (error) {
        console.log(error);
        req.flash('error', 'Lỗi!');
        res.redirect('back')
    }
}

module.exports = {
    getRegisterPage,
    dangKy,
    verifyAccount
}