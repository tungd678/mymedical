const { SuDungKhuyenMai, PhongKham, User, KhuyenMai } = require("../models");
const sequelize = require("sequelize");

async function getVoucherInfo(req, res) {
  try {
    const voucher = await SuDungKhuyenMai.findAll({
      attributes: [[sequelize.fn('to_char', sequelize.col("NgaySuDung"), "DD Mon YYYY"), 'NgaySuDung'], "SoTienDuocGiam"],
      include: [
        {
          model: PhongKham,
          attributes: ["Ten"]
        },
        {
          model: User,
          attributes: ["Username"]
        },
        {
          model: KhuyenMai,
          attributes: ["Code"]
        }
      ]
    });

    const limit = 7;
    const page = parseInt(req.query.page) || 1;
    const offset = (page - 1) * limit;

    const pagination = {
      page,
      limit,
      totalRows: voucher.length
    };
    let vouchers = voucher.slice(offset, offset + limit);

    res.render("adminVoucher", { layout: "adminLayout.hbs", vouchers, pagination });
  } catch (error) {
    console.log(error);
  }
}

module.exports = {
  getVoucherInfo
};
