const {
	PhongKham,
	ChiTietDiaChi,
	QuanHuyen,
	LamViecTai,
	User,
	GomCacKhoa,
	ChuyenKhoa,
	Feedback,
	LichHen,
	TinhThanhPho
} = require('../models');
const { Op } = require('sequelize');
const sequelize = require('sequelize');
const fs = require('fs-extra');

module.exports.getPhongKham = async (req, res) => {
	try {
		const id = req.params.id;

		const phongKham = await PhongKham.findOne({
			where: { id }
		});

		const chiTietDiaChi = await ChiTietDiaChi.findOne({
			attributes: ['QuanHuyenId', 'Duong'],
			where: { PhongKhamId: id }
		});

		const quanHuyen = await QuanHuyen.findOne({
			attributes: ['path_with_type'],
			where: { id: chiTietDiaChi.QuanHuyenId }
		});

		const BacSi = await LamViecTai.findAll({
			where: {
				PhongKhamId: id,
				TrangThai: true
			},
			include: [
				{
					model: User,
					attributes: ['HoTen', 'BangCap', 'Avatar']
				}
			]
		});

		const chuyenKhoa = await GomCacKhoa.findAll({
			where: { PhongKhamId: id },
			include: [
				{
					model: ChuyenKhoa,
					attributes: ['Ten']
				}
			]
		});

		phongKham.ChuyenKhoa = chuyenKhoa.map((i) => i.ChuyenKhoa);

		phongKham.BacSi = BacSi.map((i) => i.User);

		phongKham.DiaChi = `${chiTietDiaChi.Duong}, ${quanHuyen.path_with_type}`;

		res.render('phongKham', {
			phongKham
		});
	} catch (error) {
		console.log(error);
	}
}

module.exports.getFeedback = async (req, res) => {
	try {
		const id = req.params.id;
		const limit = 5;
		const page = parseInt(req.query.page) || 1;
		const offset = (page - 1) * limit;

		const phongKham = await PhongKham.findOne({ where: { id } });

		const feedback = await Feedback.findAll({
			where: { PhongKhamId: id },
			order: [['id', 'DESC']],
			include: [
				{
					model: User,
					attributes: ['Username', 'Avatar']
				}
			]
		});

		const chiTietDiaChi = await ChiTietDiaChi.findOne({
			attributes: ['QuanHuyenId', 'Duong'],
			where: { PhongKhamId: id }
		});

		const quanHuyen = await QuanHuyen.findOne({
			attributes: ['path_with_type'],
			where: { id: chiTietDiaChi.QuanHuyenId }
		});

		phongKham.DiaChi = `${chiTietDiaChi.Duong}, ${quanHuyen.path_with_type}`;

		const pagination = {
			page,
			limit,
			totalRows: feedback.length
		}
		phongKham.Feedback = feedback.slice(offset, offset + limit);

		res.render('phongKham-feedback', { phongKham, pagination });

	} catch (error) {
		console.log(error);
		res.redirect('back');
	}
}

module.exports.getImages = async (req, res) => {
	try {
		const id = req.params.id;

		const phongKham = await PhongKham.findOne({ where: { id } });

		const chiTietDiaChi = await ChiTietDiaChi.findOne({
			attributes: ['QuanHuyenId', 'Duong'],
			where: { PhongKhamId: id }
		});

		const quanHuyen = await QuanHuyen.findOne({
			attributes: ['path_with_type'],
			where: { id: chiTietDiaChi.QuanHuyenId }
		});

		phongKham.DiaChi = `${chiTietDiaChi.Duong}, ${quanHuyen.path_with_type}`;

		const galleryDir = 'public' + phongKham.Gallery;
		phongKham.Gallery = fs.readdirSync(galleryDir).map((i) => `${phongKham.Gallery}/${i}`);
		phongKham.Gallery.shift();

		res.render('phongKham-images', { phongKham });
	} catch (error) {
		console.log(error);
		res.redirect('back');
	}
}

module.exports.getLichHen = async (req, res) => {
	try {
		const id = req.params.id;

		const phongKham = await PhongKham.findOne({ where: { id } });

		const BacSi = await LamViecTai.findAll({
			where: {
				PhongKhamId: id,
				TrangThai: true
			},
			include: [
				{
					model: User,
					attributes: ['HoTen', 'BangCap', 'Avatar']
				}
			]
		});

		const chuyenKhoa = await GomCacKhoa.findAll({
			where: { PhongKhamId: id },
			include: [
				{
					model: ChuyenKhoa,
					attributes: ['Ten']
				}
			]
		});

		const chiTietDiaChi = await ChiTietDiaChi.findOne({
			attributes: ['QuanHuyenId', 'Duong'],
			where: { PhongKhamId: id }
		});

		const quanHuyen = await QuanHuyen.findOne({
			attributes: ['path_with_type'],
			where: { id: chiTietDiaChi.QuanHuyenId }
		});

		phongKham.DiaChi = `${chiTietDiaChi.Duong}, ${quanHuyen.path_with_type}`;

		phongKham.ChuyenKhoa = chuyenKhoa.map((i) => i.ChuyenKhoa);

		phongKham.BacSi = BacSi.map((i) => i.User);

		res.render('phongKham-lichhen', { phongKham });
	} catch (error) {
		console.log(error);
		res.redirect('back');
	}
}

module.exports.getQuanLyPhongKham = async (req, res) => {
	try {
		const id = req.params.id;

		const phongKham = await PhongKham.findOne({
			where: { id },
			include: [
				{
					model: ChiTietDiaChi,
					include: [
						{
							model: QuanHuyen,
							attributes: ['path_with_type']
						}
					]
				}
			]
		});

		const result = await Promise.all([
			LamViecTai.findAll({
				where: {
					PhongKhamId: id,
					TrangThai: true
				},
				include: [
					{
						model: User,
						attributes: ['id', 'HoTen', 'Avatar']
					}
				]
			}),
			TinhThanhPho.findAll({ attributes: ['id', 'name'] })
		]);

		const galleryDir = 'public' + phongKham.Gallery;
		phongKham.Gallery = fs.readdirSync(galleryDir).map((i) => `${phongKham.Gallery}/${i}`);
		phongKham.Gallery.shift();

		phongKham.BacSi = result[0].map((i) => i.User);

		res.render('quanLyPhongKham', {
			phongKham,
			tinhThanhPho: result[1]
		});
	} catch (error) {
		console.log(error);
	}
}

module.exports.getQuanLyImages = async (req, res) => {
	try {
		const id = req.params.id;

		const phongKham = await PhongKham.findOne({
			where: { id },
			include: [
				{
					model: ChiTietDiaChi,
					include: [
						{
							model: QuanHuyen,
							attributes: ['path_with_type']
						}
					]
				}
			]
		});

		const galleryDir = 'public' + phongKham.Gallery;
		phongKham.Gallery = fs.readdirSync(galleryDir).map((i) => `${phongKham.Gallery}/${i}`);
		phongKham.Gallery.shift();

		res.render('quanLyPhongKham-images', { phongKham });
	} catch (error) {
		console.log(error);
		res.redirect('back');
	}
}

module.exports.getQuanLyNhanVien = async (req, res) => {
	try {
		const id = req.params.id;

		const phongKham = await PhongKham.findOne({
			where: { id },
			include: [
				{
					model: ChiTietDiaChi,
					include: [
						{
							model: QuanHuyen,
							attributes: ['path_with_type']
						}
					]
				}
			]
		});

		const nhanVien = await LamViecTai.findAll({
			where: {
				PhongKhamId: id,
				TrangThai: true
			},
			include: [
				{
					model: User,
					attributes: ['id', 'HoTen', 'Avatar']
				}
			]
		});

		phongKham.BacSi = nhanVien.map((i) => i.User);

		res.render('quanLyPhongKham-nhanvien', { phongKham });
	} catch (error) {
		console.log(error);
		res.redirect('back');
	}
}

module.exports.getQuanLyLichHen = async (req, res) => {
	try {
		const id = req.params.id;
		const limit = 5;
		const page = parseInt(req.query.page) || 1;
		const offset = (page - 1) * limit;
		const date = req.query.date || new Date().toISOString().slice(0, 10);;

		const phongKham = await PhongKham.findOne({
			where: { id },
			include: [
				{
					model: ChiTietDiaChi,
					include: [
						{
							model: QuanHuyen,
							attributes: ['path_with_type']
						}
					]
				}
			]
		});

		phongKham.LichHen = await LichHen.findAll({
			where: {
				PhongKhamId: id,
				where: sequelize.where(sequelize.fn('date', sequelize.col('ThoiGianKham')), '=', date)
			}
		});

		const pagination = {
			page,
			limit,
			totalRows: phongKham.LichHen.length
		}

		phongKham.LichHen.slice(offset, offset + limit);

		res.render('quanLyPhongKham-lichhen', { phongKham, pagination, date });
	} catch (error) {
		console.log(error);
		res.redirect('back');
	}
}

module.exports.datLich = async (req, res) => {
	try {
		const result = await LichHen.create({
			// NgayKham: req.body.date,
			// GioKham: req.body.time,
			HoTen: req.body.fullname,
			SoDienThoai: req.body.phone,
			Email: req.body.email,
			ThoiGianKham: `${req.body.date} ${req.body.time}`,
			GhiChu: req.body.note,
			BacSi: req.body.doctor,
			KhoaKhamBenh: req.body.faculty,
			TrangThai: 1,
			UserId: req.user.id,
			PhongKhamId: req.params.id
		});

		// flash msg
		req.flash('success', 'Bạn đã đặt lịch hẹn thành công!');

		res.redirect('back');
	} catch (error) {
		console.log(error);
		req.flash('error', 'Lỗi! không thể đặt lịch hẹn!');
		res.redirect('back');
	}
}

module.exports.handleLichHen = async (req, res) => {
	try {
		const id = req.params.LichHenId;
		const TrangThai = parseInt(req.body.action);

		const lichHen = await LichHen.findOne({ where: { id } });

		if (!lichHen) {
			req.flash('error', 'Lỗi!!');
			return res.redirect('back');
		}

		lichHen.update({TrangThai});

		req.flash('success', 'Cập nhật thành công!')
		res.redirect('back');
	} catch (error) {
		console.log(error);
		req.flash('error', 'Lỗi!!');
		res.redirect('back');
	}
}

module.exports.capNhatThongTinChung = async (req, res) => {
	try {
		const thongTinMoi = {
			Ten: req.body.Ten,
			SoDienThoai: req.body.SoDienThoai,
			GioiThieu: req.body.GioiThieu,
			FanpageFB: req.body.FanpageFB
		}
		const diaChiMoi = {
			Duong: req.body.Duong,
			TinhThanhPhoId: req.body.TinhThanhPhoId,
			QuanHuyenId: req.body.QuanHuyenId
		}

		await PhongKham.update(thongTinMoi, { where: { id: req.params.id } });
		await ChiTietDiaChi.update(diaChiMoi, { where: { PhongKhamId: req.params.id } });

		// flasg message
		req.flash('success', 'Cập nhật thông tin thành công!');

		res.redirect('back');
	} catch (error) {
		console.log(error);
		req.flash('error', 'Đã có lỗi xảy ra!');
		res.redirect('back');
	}
}

module.exports.capNhatDanhSachNhanVien = async (req, res) => {
	try {
		const PhongKhamId = req.params.id;
		const dsNhanVien = req.body;

		// xoa het cac instance lam viec tai
		await LamViecTai.destroy({ where: { PhongKhamId } });

		for (let i = 0; i < dsNhanVien.length; i++) {
			const BacSiId = dsNhanVien[i];
			await LamViecTai.create({
				TrangThai: true,
				BacSiId,
				PhongKhamId
			});
		}

		// flasg message
		req.flash('success', 'Cập nhật danh sách nhân viên thành công!');

		res.send('ok');
	} catch (error) {
		console.log(error);
		req.flash('error', 'Đã có lỗi xảy ra!');
		res.redirect('back');
	}
}

module.exports.postGallery = (req, res) => {
	console.log('\x1b[33m%s\x1b[0m', 'post gallery');
	let img = req.files.file;
	let path = `public/images/buffer/${req.files.file.name}`;

	img.mv(path, err => {
		if (err) {
			console.log(err);
			return res.sendStatus(404);
		}

	});

	res.sendStatus(200);
}

module.exports.uploadGallery = async (req, res) => {
	try {
		const bufferPath = 'public/images/buffer';
		const pkImgPath = `public/images/phongKham/${req.params.id}/gallery`;
		const images = fs.readdirSync(bufferPath);

		for (let i = 0; i < images.length; i++) {
			const image = images[i];
			fs.moveSync(`${bufferPath}/${image}`, `${pkImgPath}/${image}`);
		}

		// xoa tat ca hinh trong buffer
		fs.emptyDirSync('public/images/buffer');

		// flasg message
		req.flash('success', 'Upload hình ảnh thành công!');

		res.redirect('back');
	} catch (error) {
		console.log(error);
		// res.sendStatus(500);
		req.flash('error', 'Đã có lỗi xảy ra!');
		res.redirect('back');
	}
}

module.exports.xoaAnh = (req, res) => {
	try {
		const fileName = 'public' + req.body.file;
		fs.removeSync(fileName);
		// flasg message
		req.flash('success', 'Xóa ảnh thành công!');
		res.redirect('back');
	} catch (error) {
		console.log(error);
		req.flash('error', 'Đã có lỗi xảy ra!');
		res.redirect('back');
	}
}

module.exports.uploadAvatar = async (req, res) => {
	try {
		const id = req.params.id;
		const phongKham = await PhongKham.findOne({ where: { id } });
		const avatar = phongKham.Avatar;
		const image = req.files.file;
		const newFilePath = `/images/phongKham/${id}/${image.name}`;

		if (avatar) {
			fs.unlinkSync(`public${avatar}`);
		}

		image.mv('public' + newFilePath, err => {
			if (err) {
				console.log(err);
			}
		})

		await phongKham.update({ Avatar: newFilePath });

		// flasg message
		req.flash('success', 'Cập nhật avatar thành công!');

		res.redirect('back');
	} catch (error) {
		console.log(error);
		req.flash('error', 'Đã có lỗi xảy ra!');
		res.redirect('back');
	}
}

module.exports.uploadThumbnail = async (req, res) => {
	try {
		const id = req.params.id;
		const phongKham = await PhongKham.findOne({ where: { id } });
		const thumbnail = phongKham.Thumbnail;
		const image = req.files.thumbnail;
		const newFilePath = `/images/phongKham/${id}/${image.name}`;

		if (thumbnail) {
			fs.unlinkSync(`public${thumbnail}`);
		}

		image.mv('public' + newFilePath, err => {
			if (err) {
				console.log(err);
			}
		})

		await phongKham.update({ Thumbnail: newFilePath });

		// flash message
		req.flash('success', 'Cập nhật thumbnail thành công!');

		res.redirect('back');
	} catch (error) {
		console.log(error);
		req.flash('error', 'Đã có lỗi xảy ra!');
		res.redirect('back');
	}
}

module.exports.uploadFeedback = async (req, res) => {
	try {
		const NoiDung = req.body.feedback;
		if (NoiDung && NoiDung !== '') {
			const result = await Feedback.create({
				NoiDung,
				ThoiGian: new Date(),
				SoDiem: 5,
				UserId: req.user.id,
				PhongKhamId: req.params.id
			});
		}

		res.redirect('back');
	} catch (error) {
		console.log(error);
		req.flash('error', 'Đã có lỗi xảy ra!');
		res.redirect('back');
	}
}