const { PhongKham, ChiTietDiaChi, QuanHuyen, GomCacKhoa, ChuyenKhoa, LamViecTai, QuangCao } = require('../models');
const { Op } = require('sequelize');
const sequelize = require('sequelize');
const models = require('../models');

async function timPhongKham(req, res) {
    try {
        const query = req.query.search;
        const limit = 5;
		const page = parseInt(req.query.page) || 1;
		const offset = (page - 1) * limit;
        let phongKham = [];

        if (query) {
            console.log(query);
            phongKham = await PhongKham.findAll({
                where: {
                    Ten: {
                        [Op.iLike]: `%${query}%`
                    }
                },
                include: [
                    {
                        model: ChiTietDiaChi,
                        attributes: ['QuanHuyenId', 'Duong'],
                        include: [
                            {
                                model: QuanHuyen,
                                attributes: ['path_with_type']
                            }
                        ]
                    }
                ]
            });
        } else if (req.query.ChuyenKhoaId && req.query.TinhThanhPhoId && req.query.QuanHuyenId) {
            const queryGomCacKhoa = models.sequelize.dialect.QueryGenerator.selectQuery('GomCacKhoas', {
                attributes: ['PhongKhamId'],
                where: { ChuyenKhoaId: req.query.ChuyenKhoaId },
            }).slice(0, -1);
            let result = await ChiTietDiaChi.findAll({
                where: {
                    TinhThanhPhoId: req.query.TinhThanhPhoId,
                    QuanHuyenId: req.query.QuanHuyenId,
                    PhongKhamId: { [Op.in]: sequelize.literal(`(${queryGomCacKhoa})`) }
                },
                include: [
                    { model: PhongKham },
                    {
                        model: QuanHuyen,
                        attributes: ['path_with_type']
                    }
                ]
            });

            // return console.log(result);
            phongKham = result.map(i => {
                return {
                    ...i.PhongKham.dataValues,
                    ChiTietDiaChi: {
                        Duong: i.Duong,
                        QuanHuyen: i.QuanHuyen
                    }
                }
            });
        }

        const pagination = {
            page,
			limit,
			totalRows: phongKham.length
        }

        phongKham = phongKham.slice(offset, offset + limit);

        for (let i = 0; i < phongKham.length; i++) {
            const p = phongKham[i];
            const result = await Promise.all([
                GomCacKhoa.findAll({
                    where: {
                        PhongKhamId: p.id
                    },
                    include: [
                        {
                            model: ChuyenKhoa,
                            attributes: ['Ten']
                        }
                    ]
                }),
                LamViecTai.findAndCountAll({
                    where: {
                        PhongKhamId: p.id
                    },
                    attributes: []
                })
            ]);

            p.chuyenKhoa = result[0];
            p.SLBacSi = result[1].count;
        }

        const quangCao = await QuangCao.findAll({
            where: {
                TrangThai: true
            },
            attributes: ['id', 'HinhAnh']
        });

        res.render('timKiem', {
            phongKham,
            quangCao,
            pagination
        });
    } catch (error) {
        console.log(error);
        res.redirect('back');
    }
}

module.exports = {
    timPhongKham
}