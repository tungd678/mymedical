async function getServicePlan(req, res) {
    res.render('adminServicePlan', {layout: 'adminLayout.hbs'});
}

module.exports = {
    getServicePlan
}