const { User } = require('../models');

function getChangeInforSuccessPage(req, res) {
    res.render('User-changeInforSuccess');
}

module.exports = {
    getChangeInforSuccessPage
}