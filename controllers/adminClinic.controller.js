const { PhongKham, GoiDichVu, ChiTietDiaChi, QuanHuyen } = require("../models");
const sequelize = require("sequelize");

module.exports.getClinicInfo = async (req, res) => {
  try {
    const phongKham = await PhongKham.findAll({
      attributes: ["id", "Ten", "SoDienThoai", "DiemTrungBinh"]
    });

    for (var i = 0; i < phongKham.length; i++) {
      const chiTietDiaChi = await ChiTietDiaChi.findOne({
        attributes: ["QuanHuyenId", "Duong"],
        where: { PhongKhamId: phongKham[i].id }
      });

      const quanHuyen = await QuanHuyen.findOne({
        attributes: ["path_with_type"],
        where: { id: chiTietDiaChi.QuanHuyenId }
      });

      phongKham[i].DiaChi = `${chiTietDiaChi.Duong}, ${
        quanHuyen.path_with_type
      }`;
    }

    const limit = 7;
    const page = parseInt(req.query.page) || 1;
    const offset = (page - 1) * limit;

    const pagination = {
      page,
      limit,
      totalRows: phongKham.length
    };
    let phongkham = phongKham.slice(offset, offset + limit);

    res.render("adminClinic", {
      layout: "adminLayout.hbs",
      phongkham,
      pagination
    });
  } catch (error) {
    console.log(error);
  }
};

module.exports.xoaClinic = async (req, res) => {
  try {
    const id = req.params.clinicId;

    PhongKham.destroy({ where: { id } });
    res.redirect("back");
  } catch (error) {
    console.log(error);
  }
};
