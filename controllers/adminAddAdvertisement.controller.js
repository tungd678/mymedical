const { QuangCao } = require("../models");
const sequelize = require("sequelize");

module.exports.getAddAdvertisement = async (req, res) => {
  try {
    res.render("adminAddAdvertisement", { layout: "adminLayout.hbs" });
  } catch (error) {
    console.log(error);
  }
}

module.exports.addAdvertisement = async (req, res) => {
  try {
    const images = req.files.image;
    const newFilePath = `/images/quangCao/${images.name}`;
    
    images.mv('public' + newFilePath, err => {
			if (err) {
				console.log(err);
			}
    });
    
    const result = await QuangCao.create({
      Ten: req.body.tenQuangCao,
      TrangThai: true,
      ThoiGianBatDau: `${req.body.dateStart}`,
      ThoiGianKetThuc: `${req.body.dateEnd}`,
      Loai: req.body.loaiQuangCao,
      HinhAnh: newFilePath
    });

    res.redirect("back");
  } catch (error) {
    console.log(error);
  }
}