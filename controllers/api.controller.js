const { PhongKham, QuanHuyen, User, LamViecTai } = require('../models');
const models = require('../models');
const sequelize = require('sequelize');

module.exports.getDanhSachPhongKham = async (req, res) => {
    try {
        let danhSachPhongKham = await PhongKham.findAll({
            attributes: ['Ten']
        });

        // danhSachPhongKham = danhSachPhongKham.map(i => i.Ten);

        res.json(danhSachPhongKham);
    } catch (error) {
        console.log(error);
    }
}

module.exports.getQuanHuyen = async (req, res) => {
    try {
        const TinhThanhPhoId = req.params.id;
        const quanHuyen = await QuanHuyen.findAll({ where: { TinhThanhPhoId }, attributes: ['id', 'name'] });

        res.json(quanHuyen);
    } catch (error) {
        console.log(error);
    }
}

module.exports.getDanhSachBacSi = async (req, res) => {
    try {
        const danhSachBacSi = await User.findAll({
            where: { LoaiUser: 2 },
            attributes: ['id', 'Avatar', 'HoTen']
        });

        res.json(danhSachBacSi);
    } catch (error) {
        console.log(error);
    }
}

module.exports.getDanhSachBacSiKhongOTrongPhongKham = async (req, res) => {
    try {
        const dsBacSiTrongPhongKham = models.sequelize.dialect.QueryGenerator.selectQuery('LamViecTais', {
            attributes: ['BacSiId'],
            where: { PhongKhamId: req.params.id }
        })
            .slice(0, -1);

        const danhSachBacSi = await User.findAll({
            where: {
                LoaiUser: 2,
                id: { [sequelize.Op.notIn]: sequelize.literal(`(${dsBacSiTrongPhongKham})`) }
            },
            attributes: ['id', 'Avatar', 'HoTen']
        });

        res.json(danhSachBacSi);
    } catch (error) {
        console.log(error);
    }
}