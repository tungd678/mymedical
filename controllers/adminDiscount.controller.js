const { KhuyenMai } = require("../models");
const sequelize = require("sequelize");

module.exports.getDiscountInfo = async (req, res) => {
  try {
    let khuyenMai = await KhuyenMai.findAll({
      attributes: [
        "id",
        "Ten",
        "TrangThai",
        [
          sequelize.fn(
            "to_char",
            sequelize.col("ThoiGianBatDau"),
            "DD Mon YYYY"
          ),
          "ThoiGianBatDau"
        ],
        [
          sequelize.fn(
            "to_char",
            sequelize.col("ThoiGianKetThuc"),
            "DD Mon YYYY"
          ),
          "ThoiGianKetThuc"
        ],
        "MucGiam",
        "Code"
      ]
    });

    const limit = 7;
    const page = parseInt(req.query.page) || 1;
    const offset = (page - 1) * limit;

    const pagination = {
      page,
      limit,
      totalRows: khuyenMai.length
    };
    let khuyenmai = khuyenMai.slice(offset, offset + limit);

    res.render("adminDiscount", {
      layout: "adminLayout.hbs",
      khuyenmai,
      pagination
    });
  } catch (error) {
    console.log(error);
  }
};

module.exports.xoaDiscount = async (req, res) => {
  try {
    const id = req.params.discountId;
    KhuyenMai.destroy({ where: { id } });
    res.redirect("back");
  } catch (error) {
    console.log(error);
  }
};
