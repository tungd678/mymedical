const { User } = require('../models');
const bcrypt = require('bcryptjs');
const sequelize = require('sequelize');

async function getSetInforPage(req, res) {
	try {
		const id = req.params.id;

		const nguoiDung = await User.findOne({
			where: {
				id
			},
			attributes: [ 'id','Username', 'HoTen', 'Password', 'SoDienThoai', 'Email', 'GioiTinh', 'NgaySinh', 'Avatar' ]
		});
		if (nguoiDung) {
			res.render('User-SetInfor', {
				nguoiDung
			});
		} else {
			res.redirect('back');
		}
	} catch (error) {
		console.log(error);
	}
}

async function updateUser(req, res){
	try{
		const id = req.params.id;
		await User.update(
			{
				Username: req.body.Username,
				HoTen: req.body.HoTen,
				Email: req.body.Email,
				SoDienThoai: req.body.SoDienThoai,
				GioiTinh: req.body.GioiTinh,
				NgaySinh: req.body.NgaySinh,
			}
			,
			{where: {id}},

		)
		.then(function(){
			res.redirect('/');
		})
		.catch(function(error){
			res.json(error);
		})
		// res.render('User-SetInfor');
	}catch(error){
		console.log(error);
		// res.redirect('back');
	}
}

module.exports = {
	getSetInforPage, updateUser
};
