const express = require("express");
const router = express.Router();
const controller = require("../controllers/adminDiscount.controller");

router.get("/discount", controller.getDiscountInfo);
router.delete("/discount/:discountId/delete", controller.xoaDiscount);

module.exports = router;
