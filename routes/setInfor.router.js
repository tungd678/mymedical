const express = require('express');
const router = express.Router();
const middewares = require('../middlewares');
const setInforController = require('../controllers/setInfor.controller');

router.get('/:id', middewares.checkUserInfo, setInforController.getSetInforPage);
router.post('/:id/doiTT',setInforController.updateUser );
module.exports = router;