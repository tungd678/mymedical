const express = require('express');
const router = express.Router();
const controller = require('../controllers/lichHen.controller');

router.get('/:id', controller.getAppointmentSchedulePage);

module.exports = router;