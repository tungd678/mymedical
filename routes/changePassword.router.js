const express = require('express');
const router = express.Router();
const setChangePassword = require('../controllers/changePassword.controller');

router.get('/:id', setChangePassword.getChangePasswordPage);

router.post('/:id/changePass', setChangePassword.updatePassword);

module.exports = router;