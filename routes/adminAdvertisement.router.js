const express = require("express");
const router = express.Router();
const controller = require("../controllers/adminAdvertisement.controller");

router.get("/advertisement", controller.getAdvertisement);
router.delete("/advertisement/:adId/delete", controller.xoaAd);

module.exports = router;
