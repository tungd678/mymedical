const express = require('express');
const router = express.Router();
const controller = require('../controllers/adminUser.controller');

router.get('/user', controller.getUserInfo);
router.post('/user/:userId', controller.updateTrangThai);
router.delete('/user/:userId/delete', controller.xoaUser);
module.exports = router;