const express = require('express');
const router = express.Router();
const controller = require('../controllers/adminDoctor.controller');

router.get('/doctor', controller.getDoctorInfo);
router.post('doctor/:bacsiId', controller.updateTrangThai);
router.delete('doctor/:bacsiId/delete', controller.xoaBacSi);

module.exports = router;