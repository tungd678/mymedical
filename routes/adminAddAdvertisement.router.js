const express = require("express");
const router = express.Router();
const controller = require("../controllers/adminAddAdvertisement.controller");

router.get("/advertisement/add", controller.getAddAdvertisement);
router.post("/advertisement/add", controller.addAdvertisement);

module.exports = router;