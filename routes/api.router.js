const express = require('express');
const router = express.Router();
const apiController = require('../controllers/api.controller');

router.get('/danh-sach-phong-kham.json', apiController.getDanhSachPhongKham);
router.get('/danh-sach-bac-si.json', apiController.getDanhSachBacSi);
router.get('/danh-sach-bac-si/:id.json', apiController.getDanhSachBacSiKhongOTrongPhongKham);
router.post('/quan-huyen/:id.json', apiController.getQuanHuyen);

module.exports = router;