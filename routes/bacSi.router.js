const express = require('express');
const router = express.Router();
const controller = require('../controllers/bacSi.controller');
const { checkQuanLyBacSi } = require('../middlewares');

router.get('/:id', controller.getBacSiPage);
router.get('/:id/quan-ly', checkQuanLyBacSi, controller.getQuanLyBacSi);

router.post('/:id/quan-ly', checkQuanLyBacSi, controller.capNhapThongTinBacSi);

module.exports = router;