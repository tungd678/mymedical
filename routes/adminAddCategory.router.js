const express = require("express");
const router = express.Router();
const controller = require("../controllers/adminAddCategory.controller");

router.get("/category/add", controller.getAddCategory);
router.post("/category/add", controller.addCategory);

module.exports = router;