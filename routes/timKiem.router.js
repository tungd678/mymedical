const express = require('express');
const router = express.Router();
const controller = require('../controllers/timKiem.controller');

router.get('/', controller.timPhongKham);

module.exports = router;