const express = require('express');
const router = express.Router();
const registerController = require('../controllers/register.controller');
const { isNotLoggedIn } = require('../middlewares');

router.get('/', isNotLoggedIn, registerController.getRegisterPage);
router.get('/verification', registerController.verifyAccount)

router.post('/', registerController.dangKy);

module.exports = router;