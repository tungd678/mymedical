const express = require('express');
const router = express.Router();
const controller = require('../controllers/adminCategory.controller');

router.get('/category', controller.getCategory);
router.delete('/category/:chuyenkhoaId/delete', controller.xoaCategory);
router.get('/category/search', controller.searchCategory);

module.exports = router;