const express = require("express");
const router = express.Router();
const getVoucherController = require("../controllers/adminVoucher.controller");

router.get("/voucher", getVoucherController.getVoucherInfo);

module.exports = router;