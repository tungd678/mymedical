const express = require('express');
const router = express.Router();
const passport = require('passport');
const loginController = require('../controllers/login.controller');
const { isNotLoggedIn } = require('../middlewares');


router.get('/', isNotLoggedIn, loginController.getLoginPage);

router.get('/facebook', passport.authenticate('facebook', { scope: ['email'] }));
router.get('/facebook/callback', passport.authenticate('facebook', {
    successRedirect: '/',
    failureRedirect: '/dang-nhap'
}), loginController.auth);

router.get('/google', passport.authenticate('google', { scope: ['profile'] }));
router.get('/google/callback', passport.authenticate('google', {
    successRedirect: '/',
    failureRedirect: '/dang-nhap'
}));

router.post('/', passport.authenticate('local', {
    failureRedirect: '/dang-nhap',
    // successRedirect: req.headers.referer
    failureFlash: true
}), loginController.auth);




module.exports = router;