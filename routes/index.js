const express = require('express');
const router = express.Router();
const middewares = require('../middlewares');
const homepageRouter = require('./homepage.router');
const phongKhamRouter = require('./phongKham.router');
const loginRouter = require('./login.router');
const adminCategoryRouter = require('./adminCategory.router');
const adminAdvertisementRouter = require('./adminAdvertisement.router');
const bacSiRouter = require('./bacSi.router');
const registerRouter = require('./register.router');

const apiRouter = require('./api.router');
const timKiemRouter = require('./timKiem.router');
const adminDashboard = require('./adminDashboard.router');
const adminServicePlan = require('./adminServicePlan.router');
const logoutRouter = require('./logout.router');
const adminUserInfo = require('./adminUser.router');
const adminDoctorInfo = require('./adminDoctor.router');
const adminClinicInfo = require('./adminClinic.router');
const adminDiscount = require('./adminDiscount.router');
const adminAddDiscount = require('./adminAddDiscount.router');
const adminAddCategory = require('./adminAddCategory.router');
const adminAddAdvertisement = require('./adminAddAdvertisement.router');
const adminVoucherInfo = require('./adminVoucher.router');

const userSetInforRouter = require('./setInfor.router');
const userChangeInforSuccess = require('./changeInforSuccess.router');
const userChangePassword = require('./changePassword.router');
const userAppointmentSchedule = require('./lichHen.router');
// router.use('/...', ...router)

router.use('/', homepageRouter);
router.use('/api', apiRouter);
router.use('/phong-kham/', phongKhamRouter);
router.use('/dang-ky', registerRouter);
router.use('/dang-nhap', loginRouter);
router.use('/dang-xuat', logoutRouter);
router.use('/bac-si', bacSiRouter);

router.use('/tim-kiem', timKiemRouter);
router.use('/admin', middewares.isLoggedIn, middewares.checkAdminAuth, adminCategoryRouter);
router.use('/admin', middewares.isLoggedIn, middewares.checkAdminAuth, adminAdvertisementRouter);
router.use('/admin', middewares.isLoggedIn, middewares.checkAdminAuth, adminDashboard);
router.use('/admin', middewares.isLoggedIn, middewares.checkAdminAuth, adminServicePlan);
router.use('/admin', middewares.isLoggedIn, middewares.checkAdminAuth, adminUserInfo);
router.use('/admin', middewares.isLoggedIn, middewares.checkAdminAuth, adminDoctorInfo);
router.use('/admin', middewares.isLoggedIn, middewares.checkAdminAuth, adminClinicInfo);
router.use('/admin', middewares.isLoggedIn, middewares.checkAdminAuth, adminDiscount);
router.use('/admin', middewares.isLoggedIn, middewares.checkAdminAuth, adminAddDiscount);
router.use('/admin', middewares.isLoggedIn, middewares.checkAdminAuth, adminAddCategory);
router.use('/admin', middewares.isLoggedIn, middewares.checkAdminAuth, adminAddAdvertisement);
router.use('/admin', middewares.isLoggedIn, middewares.checkAdminAuth, adminVoucherInfo);

router.use('/dang-ky',registerRouter);

router.use('/user/SetInfor', middewares.isLoggedIn, userSetInforRouter);
router.use('/user/changeInforSuccess', middewares.isLoggedIn, userChangeInforSuccess);
router.use('/user/changePassword', middewares.isLoggedIn, userChangePassword);
router.use('/user/AppointmentSchedule', middewares.isLoggedIn, userAppointmentSchedule);


module.exports = router;