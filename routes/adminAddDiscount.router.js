const express = require("express");
const router = express.Router();
const controller = require("../controllers/adminAddDiscount.controller");

router.get("/discount/add", controller.getAddDiscountInfo);
router.post("/discount/add", controller.addDiscountInfo);

module.exports = router;