const express = require('express');
const router = express.Router();
const dashboardController = require('../controllers/adminDashboard.controller');

router.get('/', dashboardController.getDashboard);

module.exports = router;