const express = require('express');
const router = express.Router();
const setChangeInforController = require('../controllers/changeInforSuccess.controller');

router.get('/', setChangeInforController.getChangeInforSuccessPage);

module.exports = router;