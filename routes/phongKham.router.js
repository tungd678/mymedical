const express = require('express');
const router = express.Router();
const controller = require('../controllers/phongKham.controller');
const { checkQuanLyPhongKham, isLoggedIn } = require('../middlewares');

router.get('/:id', controller.getPhongKham);
router.get('/:id/feedback', controller.getFeedback);
router.get('/:id/images', controller.getImages);
router.get('/:id/lichhen', controller.getLichHen);
router.get('/:id/quan-ly', checkQuanLyPhongKham, controller.getQuanLyPhongKham);
router.get('/:id/quan-ly/images', checkQuanLyPhongKham, controller.getQuanLyImages);
router.get('/:id/quan-ly/nhan-vien', checkQuanLyPhongKham, controller.getQuanLyNhanVien);
router.get('/:id/quan-ly/lich-hen', checkQuanLyPhongKham, controller.getQuanLyLichHen);

router.post('/:id/dat-lich', controller.datLich);
router.post('/:id/upload', controller.uploadGallery);
router.post('/:id/feedback', isLoggedIn, controller.uploadFeedback);
router.post('/:id/gallery', checkQuanLyPhongKham, controller.postGallery);
router.post('/:id/lich-hen/:LichHenId', checkQuanLyPhongKham, controller.handleLichHen);

router.put('/:id/avatar', controller.uploadAvatar);
router.put('/:id/thumbnail', controller.uploadThumbnail);
router.put('/:id/thong-tin-chung', checkQuanLyPhongKham, controller.capNhatThongTinChung);
router.put('/:id/nhan-vien', checkQuanLyPhongKham, controller.capNhatDanhSachNhanVien);

router.delete('/:id/xoa-anh', checkQuanLyPhongKham, controller.xoaAnh);

module.exports = router;
