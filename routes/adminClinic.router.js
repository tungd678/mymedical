const express = require("express");
const router = express.Router();
const controller = require("../controllers/adminClinic.controller");

router.get("/clinic", controller.getClinicInfo);
router.delete("/clinic/:clinicId/delete", controller.xoaClinic);

module.exports = router;
