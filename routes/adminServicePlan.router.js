const express = require('express');
const router = express.Router();
const servicePlanController = require('../controllers/adminServicePlan.controller');

router.get('/serviceplan', servicePlanController.getServicePlan);

module.exports = router;