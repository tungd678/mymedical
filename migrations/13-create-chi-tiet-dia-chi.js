'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('ChiTietDiaChis', { 
      PhongKhamId: {
        type: Sequelize.INTEGER,
        references: {
          model: 'PhongKhams',
          key: 'id',
        },
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
        allowNull: false
      },
      TinhThanhPhoId: {
        type: Sequelize.INTEGER,
        references: {
          model: 'TinhThanhPhos',
          key: 'id',
        },
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
        allowNull: false
      },
      QuanHuyenId: {
        type: Sequelize.INTEGER,
        references: {
          model: 'QuanHuyens',
          key: 'id',
        },
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
        allowNull: false
      },
      Duong: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
    // .then(() => {
    //   return queryInterface.removeColumn('ChiTietDiaChis', 'id');
    // })
    // .then(() => {
    //   return queryInterface.addConstraint('ChiTietDiaChis', ['PhongKhamId'], {
    //     type: 'primary key',
    //     name: 'chitietdiachi_pkey'
    //   });
    // });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('ChiTietDiaChis');
  }
};
