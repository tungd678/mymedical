'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('TinhThanhPhos', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      type: {
        type: Sequelize.STRING
      },
      name: {
        type: Sequelize.STRING
      },
      slug: {
        type: Sequelize.STRING
      },
      name_with_type: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    })
    .then(() => {
      return queryInterface.removeColumn(
        'TinhThanhPhos',
        'createdAt'
      );
    })
    .then(() => {
      return queryInterface.removeColumn(
        'TinhThanhPhos',
        'updatedAt'
      );
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('TinhThanhPhos');
  }
};