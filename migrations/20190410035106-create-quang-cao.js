'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('QuangCaos', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      Ten: {
        type: Sequelize.STRING
      },
      HinhAnh: {
        type: Sequelize.STRING
      },
      TrangThai: {
        type: Sequelize.BOOLEAN
      },
      ThoiGianBatDau: {
        type: Sequelize.DATE
      },
      ThoiGianKetThuc: {
        type: Sequelize.DATE
      },
      Loai: {
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('QuangCaos');
  }
};