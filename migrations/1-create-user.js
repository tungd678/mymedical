'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Users', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      Username: {
        type: Sequelize.STRING
      },
      Password: {
        type: Sequelize.TEXT
      },
      HoTen: {
        type: Sequelize.STRING
      },
      SoDienThoai: {
        type: Sequelize.STRING
      },
      Email: {
        type: Sequelize.STRING
      },
      GioiTinh: {
        type: Sequelize.INTEGER
      },
      NgaySinh: {
        type: Sequelize.DATE
      },
      LoaiUser: {
        type: Sequelize.INTEGER
      },
      BangCap: {
        type: Sequelize.TEXT
      },
      TrangThai: {
        type: Sequelize.BOOLEAN
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Users');
  }
};