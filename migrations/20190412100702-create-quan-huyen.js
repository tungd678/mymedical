'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('QuanHuyens', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      type: {
        type: Sequelize.STRING
      },
      name: {
        type: Sequelize.STRING
      },
      slug: {
        type: Sequelize.STRING
      },
      name_with_type: {
        type: Sequelize.STRING
      },
      path: {
        type: Sequelize.STRING
      },
      path_with_type: {
        type: Sequelize.STRING
      }
    })
    .then(() => {
      return queryInterface.removeColumn(
        'QuanHuyens',
        'createdAt',
      );
    })
    .then(() => {
      return queryInterface.removeColumn(
        'QuanHuyens',
        'updatedAt'
      );
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('QuanHuyens');
  }
};