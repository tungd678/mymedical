'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('KhuyenMais', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      Ten: {
        type: Sequelize.STRING
      },
      TrangThai: {
        type: Sequelize.BOOLEAN
      },
      ThoiGianBatDau: {
        type: Sequelize.DATE
      },
      ThoiGianKetThuc: {
        type: Sequelize.DATE
      },
      MucGiam: {
        type: Sequelize.NUMERIC
      },
      Code: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('KhuyenMais');
  }
};