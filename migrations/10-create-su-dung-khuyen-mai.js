'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
      return queryInterface.createTable('SuDungKhuyenMais', {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER
        },
        UserId: {
          type: Sequelize.INTEGER,
          references: {
            model: 'Users',
            key: 'id',
          },
          onDelete: 'CASCADE',
          onUpdate: 'CASCADE',
          allowNull: false
        },
        KhuyenMaiId: {
          type: Sequelize.INTEGER,
          references: {
            model: 'KhuyenMais',
            key: 'id',
          },
          onDelete: 'CASCADE',
          onUpdate: 'CASCADE',
          allowNull: false
        },
        PhongKhamId: {
          type: Sequelize.INTEGER,
          references: {
            model: 'PhongKhams',
            key: 'id',
          },
          onDelete: 'CASCADE',
          onUpdate: 'CASCADE',
          allowNull: false
        },
        NgaySuDung: {
          allowNull: false,
          type: Sequelize.DATE
        },
        SoTienDuocGiam: {
          allowNull: false,
          type: Sequelize.NUMERIC
        },
      })
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('SuDungKhuyenMais');
  }
};
