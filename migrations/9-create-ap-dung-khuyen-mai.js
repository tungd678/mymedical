'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('ApDungKhuyenMai', {
        KhuyenMaiId: {
          type: Sequelize.INTEGER,
          references: {
            model: 'KhuyenMais',
            key: 'id',
          },
          onDelete: 'CASCADE',
          onUpdate: 'CASCADE',
          allowNull: false
        },
        PhongKhamId: {
          type: Sequelize.INTEGER,
          references: {
            model: 'PhongKhams',
            key: 'id',
          },
          onDelete: 'CASCADE',
          onUpdate: 'CASCADE',
          allowNull: false
        }
      })
      .then(() => {
        return queryInterface.addConstraint('ApDungKhuyenMai', ['KhuyenMaiId', 'PhongKhamId'], {
          type: 'primary key',
          name: 'apdungkhuyenmai_pkey'
        });
      });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('ApDungKhuyenMais');
  }
};
