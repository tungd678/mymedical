module.exports = {
    isNotLoggedIn(req, res, next) {
        if (!req.isAuthenticated()) {
            return next();
        }

        res.redirect('/');
    },
    isLoggedIn(req, res, next) {
        if (req.isAuthenticated()) {
            return next();
        }
        // req.flash('error', 'Bạn phải đăng nhập để làm việc này!');
        res.redirect('/dang-nhap');
    },
    checkAdminAuth(req, res, next) {
        if (res.locals.currentUser.LoaiUser === 1) {
            return next();
        }
        // req.flash('error', 'Bạn không đủ quyền để làm việc này!');
        res.redirect('back');
    },
    checkUserInfo(req, res, next) {
        if (req.isAuthenticated()) {
            if ((req.params.id == req.user.id)) {
                next();
            } else {
                // req.flash('error', 'Bạn không thể truy cập tới profile này!');
                res.redirect('back');
            }
        } else {
            // req.flash('error', 'Bạn phải đăng nhập để làm việc này!');
            res.redirect('/dang-nhap');
        }
    },
    checkQuanLyPhongKham(req, res, next) {
        if (req.isAuthenticated()) {
            if (req.user.LoaiUser === 4 && req.user.PhongKhamId === parseInt(req.params.id)) {
                next();
            } else {
                // flash msg
                res.redirect('back');
            }
        } else {
            res.redirect('/dang-nhap');
        }
    },
    checkQuanLyBacSi(req, res, next) {
        if (req.isAuthenticated()) {
            if (req.user.LoaiUser === 2 && req.user.id === parseInt(req.params.id)) {
                next();
            } else {
                // flash msg
                res.redirect('back');
            }
        } else {
            res.redirect('/dang-nhap');
        }
    }
}