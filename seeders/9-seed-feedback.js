'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Feedbacks', [
      {
        NoiDung: 'Chất lượng phục vụ của trung tâm rất tốt. Phòng bệnh sạch sẽ. Từ khi điều trị ở đây tôi đã khỏi hẳn bệnh. Rất cảm ơn các bác sĩ và chúc trung tâm ngày càng phát triển.',
        ThoiGian: new Date(),
        SoDiem: 5,
        UserId: 4,
        PhongKhamId: 4,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        NoiDung: 'Gâu gâu awn … gâu gâu.',
        ThoiGian: new Date(),
        SoDiem: 5,
        UserId: 5,
        PhongKhamId: 4,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        NoiDung: 'Tôi chân thành cảm ơn đội ngũ bác sĩ đã tận tình cứu chữa tôi vượt qua cơn nguy hiểm. Chúc trung tâm thành công!',
        ThoiGian: new Date(),
        SoDiem: 5,
        UserId: 6,
        PhongKhamId: 4,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        NoiDung: '👌',
        ThoiGian: new Date(),
        SoDiem: 5,
        UserId: 7,
        PhongKhamId: 4,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        NoiDung: `because i'm batman.`,
        ThoiGian: new Date(),
        SoDiem: 5,
        UserId: 8,
        PhongKhamId: 4,
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Feedbacks', null, {});
  }
};
