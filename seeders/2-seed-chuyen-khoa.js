'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('ChuyenKhoas', [
      {
        Ten: 'Đa Khoa',
        MoTa: `<p>Đa khoa là từ dùng để chỉ một cơ sở y tế hoặc bác sĩ đảm nhiệm điều trị nhiều chuyên khoa</p>`,
        createdAt: Sequelize.literal('NOW()'),
        updatedAt: Sequelize.literal('NOW()')
      },
      {
        Ten: 'Phụ khoa',
        MoTa: `<p>Đây là chuyên khoa có chức năng chẩn khám, điều trị nội khoa lẫn phẫu thuật ngoại khoa các bệnh lý phụ khoa lành tính, các bệnh lý cấp cứu và các bệnh liên quan tới nội tiết sinh sản.</p><p>Các bệnh lý phụ khoa được ứng dụng điều trị nội tiết như rong kinh, băng huyết, vô kinh, rối loạn nội tiết (vị thành niên và mãn kinh), sẩy thai liên tiếp, điều hoà sinh sản, nạo thai dưới 3 tháng tuổi, phá thai to, hút thai khó dưới 12 tuần. Phương pháp phẫu thuật nội soi được áp dụng cắt tử cung, bóc u xơ, soi buồng tử cung,...Phương pháp ngoại khoa truyền thống được thực hiện trong phẫu thuật đường âm đạo, cắt tử cung đường âm đạo, các phẫu thuật trong dị dạng sinh dục, sa sinh dục, u nang buồng trứng,...</p>`,
        createdAt: Sequelize.literal('NOW()'),
        updatedAt: Sequelize.literal('NOW()')
      },
      {
        Ten: 'Nam Khoa',
        MoTa: `<p>Chức năng và nhiệm vụ của Nam Khoa là chuyên khám, điều trị cũng như phát hiện sớm các vấn đề sức khỏe liên quan đến cơ quan sinh dục của nam giới, các chứng bệnh sinh lý, chức năng sinh sản và bệnh lây lan qua đường tình dục.</p><p>Các căn bệnh phổ biến của khoa như: viêm tinh hoàn, viêm bao quy đầu, viêm bàng quang, viêm niệu đạo, dài bao quy đầu, hẹp bao quy đầu,...</p>`,
        createdAt: Sequelize.literal('NOW()'),
        updatedAt: Sequelize.literal('NOW()')
      },
      {
        Ten: 'Khám bệnh',
        MoTa: `<p>Khoa khám bệnh là cơ sở ban đầu trong công tác khám chữa bệnh, tiếp nhận bệnh nhân khi đến viện. Chức năng của khoa khám bệnh bao gồm: khám chữa bệnh cho mọi đối tượng có nhu cầu (BHYT đúng tuyến, tự nguyện, khám dịch vụ theo yêu cầu); khám và cấp giấy chứng nhận sức khỏe các loại; khám kiểm tra sức khỏe định kỳ cho các cá nhân và tập thể; lấy bệnh phẩm xét nghiệm tại nhà, xét nghiệm theo yêu cầu; điều trị ban ngày theo yêu cầu.</p>`,
        createdAt: Sequelize.literal('NOW()'),
        updatedAt: Sequelize.literal('NOW()')
      },
    ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('ChuyenKhoas', null, {});
  }
};
