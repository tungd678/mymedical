'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('LamViecTais', [
      {
        BacSiId: 1,
        PhongKhamId: 4,
        TrangThai: true,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        BacSiId: 2,
        PhongKhamId: 4,
        TrangThai: true,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      // {
      //   BacSiId: 3,
      //   PhongKhamId: 4,
      //   TrangThai: true,
      //   createdAt: new Date(),
      //   updatedAt: new Date()
      // },
    ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('LamViecTais', null, {});
  }
};
