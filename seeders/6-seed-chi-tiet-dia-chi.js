'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('ChiTietDiaChis', [
      {
        PhongKhamId: 1,
        TinhThanhPhoId: 79,
        QuanHuyenId: 760,
        Duong: '1, Nguyễn Thị Minh Khai, phường 1',
        createdAt: Sequelize.literal('NOW()'),
        updatedAt: Sequelize.literal('NOW()')
      },
      {
        PhongKhamId: 2,
        TinhThanhPhoId: 79,
        QuanHuyenId: 774,
        Duong: '1, Nguyễn Thị Nghĩa, phường 1',
        createdAt: Sequelize.literal('NOW()'),
        updatedAt: Sequelize.literal('NOW()')
      },
      {
        PhongKhamId: 3,
        TinhThanhPhoId: 79,
        QuanHuyenId: 768,
        Duong: '1, Nguyễn Văn Trỗi, phường 2',
        createdAt: Sequelize.literal('NOW()'),
        updatedAt: Sequelize.literal('NOW()')
      },
      {
        PhongKhamId: 4,
        TinhThanhPhoId: 79,
        QuanHuyenId: 766,
        Duong: '1100, Trường Sa, phường 11',
        createdAt: Sequelize.literal('NOW()'),
        updatedAt: Sequelize.literal('NOW()')
      },
      {
        PhongKhamId: 5,
        TinhThanhPhoId: 1,
        QuanHuyenId: 7,
        Duong: '458, Phố Minh Khai, Vĩnh Tuy',
        createdAt: Sequelize.literal('NOW()'),
        updatedAt: Sequelize.literal('NOW()')
      },
      {
        PhongKhamId: 6,
        TinhThanhPhoId: 79,
        QuanHuyenId: 766,
        Duong: '461 Cộng Hòa, 15',
        createdAt: Sequelize.literal('NOW()'),
        updatedAt: Sequelize.literal('NOW()')
      }
    ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('ChiTietDiaChis', null, {});
  }
};
