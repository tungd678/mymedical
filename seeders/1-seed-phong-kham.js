'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('PhongKhams', [
      {
        Ten: 'Bệnh viện Từ Dũ',
        SoDienThoai: '0123456789',
        // DiaChi: '1, Nguyễn Thị Minh Khai, phường 1, quận 1, Tp Hồ Chí Minh',
        Avatar: '/images/phongKham/1/avatar.jpg',
        Gallery: '/images/phongKham/1/gallery',
        Thumbnail: '/images/phongKham/1/thumbnail.jpg',
        DiemTrungBinh: 0,
        // NguoiQuanLyId: null,
        GioiThieu: `Bệnh viện Từ Dũ thành phố Hồ Chí Minh đã và đang là một địa chỉ tin cậy, ngày càng được bệnh nhân tin yêu. Đáp lại tấm chân tình ấy với những nỗ lực không mệt mỏi của ban Giám đốc và toàn thể cán bộ công chức bệnh viện trên mọi lĩnh vực, nhằm một mục tiêu giữ vững danh hiệu, cố gắng để đạt thành tích mới, hết lòng hưởng ứng phong trào: “Trung thành, sáng tạo, tận tụy, gương mẫu”. Với tinh thần đoàn kết gắn bó và cầu tiến, lòng nhiệt tình với tâm huyết người làm y đức, bệnh viện đã xây dựng mạng lưới tuyến cơ sở vững mạnh đủ khả năng phục vụ bệnh nhân tại chỗ, làm tốt công tác chăm sóc sức khỏe bà mẹ và trẻ em khu vực phía Nam.`,
        FanpageFB: 'https://www.facebook.com/BenhvienTuDu2015/',
        createdAt: Sequelize.literal('NOW()'),
        updatedAt: Sequelize.literal('NOW()')
      },
      {
        Ten: 'Bệnh viện 30/4',
        SoDienThoai: '0123456789',
        // DiaChi: '1, Nguyễn Thị Nghĩa, phường 1, quận 5, Tp Hồ Chí Minh',
        Avatar: '/images/phongKham/2/avatar.jpg',
        Gallery: '/images/phongKham/2/gallery',
        Thumbnail: '/images/phongKham/2/thumbnail.jpg',
        DiemTrungBinh: 0,
        // NguoiQuanLyId: null,
        GioiThieu: `Bệnh viện 30-4 thuộc Tổng cục Hậu cần – Kỹ thuật, Bộ Công an, là bệnh viện đa khoa hạng I, Bệnh viện đầu ngành của lực lượng công an nhân dân. Tiền thân là Bệnh xá Ban An ninh Trung ương cục miền Nam, đơn vị Anh hùng lực lượng vũ trang nhân dân. Bệnh viện có nhiệm vụ chăm sóc sức khỏe, khám chữa bệnh cho cán bộ chiến sỹ Công an, cán bộ công an nhân dân hưu trí, bảo hiểm y tế, nhân dân và các đối tượng khác; tham gia công tác nghiên cứu khoa học và giảng dạy cho học sinh – sinh viên Y khoa; thực hiện hợp tác quốc tế và khám sức khỏe cho người đi nước ngoài.\nHiện nay, bệnh viện có quy mô là 500 giường, 31 khoa phòng và 01 trung tâm, theo Quyết định 962/2012/QĐ-BCA của Bộ trưởng Bộ công an thì đến năm 2015 Bệnh viện nâng quy mô lên 600 giường và 38 khoa phòng, 1 trung tâm. Nhân sự của Bệnh viện hiện nay có hơn 650 cán bộ chiến sĩ, trong đó trên 60% cán bộ có trình độ Đại học và trên Đại học được đào tạo trong và ngoài nước như Chuyên khoa I, Chuyên khoa II, Thạc sĩ, Tiến sĩ.`,
        FanpageFB: 'https://www.facebook.com/benhviendaihocyduoc/',
        createdAt: Sequelize.literal('NOW()'),
        updatedAt: Sequelize.literal('NOW()')
      },
      {
        Ten: 'Bệnh viện Nhi Đồng',
        SoDienThoai: '0333666999',
        // DiaChi: '1, Nguyễn Văn Trỗi, phường 2, quận Phú Nhuận, Tp Hồ Chí Minh',
        Avatar: '/images/phongKham/3/avatar.jpg',
        Gallery: '/images/phongKham/3/gallery',
        Thumbnail: '/images/phongKham/3/thumbnail.jpg',
        DiemTrungBinh: 0,
        // NguoiQuanLyId: null,
        GioiThieu: 'Bệnh viện Nhi đồng 1 là bệnh viện chuyên khoa nhi, được xây dựng năm 1954 và chính thức hoạt động vào tháng 10 năm 1956 với 268 giường bệnh nội trú. Trải qua hơn 58 năm hoạt động, Bệnh viện chúng tôi ngày càng phát triển vững mạnh với quy mô 1.400 giường nội trú, hơn 1.600 nhân viên; Bệnh viện thu dung trên 1,5 triệu lượt khám và 95.000 lượt điều trị nội trú hàng năm. Hiện nay, chúng tôi tiếp nhận điều trị tất cả các trẻ bệnh từ mới sinh đến 15 tuổi ở thành phố Hồ Chí Minh và các tỉnh.',
        FanpageFB: 'https://www.facebook.com/benhviennhidongthanhpho/',
        createdAt: Sequelize.literal('NOW()'),
        updatedAt: Sequelize.literal('NOW()')
      },
      {
        Ten: 'Phòng khám Men\'s Health',
        SoDienThoai: '0333111222',
        // DiaChi: '1100, Trường Sa, phường 11, quận Tân Bình, Tp Hồ Chí Minh',
        Avatar: '/images/phongKham/4/avatar.png',
        Gallery: '/images/phongKham/4/gallery',
        Thumbnail: '/images/phongKham/4/thumbnail.jpg',
        DiemTrungBinh: 0,
        // NguoiQuanLyId: null,
        GioiThieu: `Trung tâm sức khỏe Nam giới “Men’s Health” là phòng khám Nam Khoa chuyên nghiệp đầu tiên tại Việt Nam cung cấp các dịch vụ tư vấn và điều trị bệnh nam giới theo tiêu chuẩn Hoa Kỳ với chi phí hợp lý bằng các trang thiết bị hiện đại nhập từ các nước tiên tiến như Đức, Pháp, Hoa Kỳ do đội ngũ Y - Bác sĩ đầu ngành tại Việt Nam trong lĩnh vực sức khỏe nam giới trực tiếp tư vấn sức khỏe và điều trị. Ngoài ra, Trung tâm còn có mạng lưới cộng tác và hỗ trợ hội chẩn từ các Bác sĩ, Chuyên gia uy tín trên thế giới trong các trường hợp bệnh lý phức tạp như: chuyển giới, rối loạn giới tính\nVới chi phí hợp lý cùng các trang thiết bị hiện đại nhập từ các nước tiên tiến như Đức, Pháp, Hoa Kỳ được những chuyên gia đầu ngành của Việt Nam về Nam khoa, Niệu khoa, Nội tiết, Tim mạch, Da liễu trong lĩnh vực sức khỏe Nam giới phối hợp trực tiếp tư vấn sức khỏe và điều trị.`,
        FanpageFB: 'https://www.facebook.com/Mens-Health-HCM-729206213883761/',
        createdAt: Sequelize.literal('NOW()'),
        updatedAt: Sequelize.literal('NOW()')
      },
      {
        Ten: 'Hệ thống Y tế Vinmec',
        SoDienThoai: '0339743550',
        // DiaChi: '458 Phố Minh Khai, Vĩnh Tuy, Hai Bà Trưng, Hà Nội',
        Avatar: '/images/phongKham/5/avatar.jpg',
        Gallery: '/images/phongKham/5/gallery',
        Thumbnail: '/images/phongKham/5/thumbnail.jpg',
        DiemTrungBinh: 0,
        // NguoiQuanLyId: null,
        GioiThieu: `Ngày 8 tháng 2 năm 2015, hệ thống y tế Vinmec chính thức khai trương phòng khám Đa khoa Quốc tế Vinmec Sài Gòn tại 2/2 đường Trần Cao Vân, phường Đa Kao, quân 1, thành phố Hồ Chí Minh. Đây là Phòng khám Đa khoa Quốc tế đầu tiên tại thành phố Hồ Chí Minh trang bị máy chụp cộng hưởng từ MRI 1.5T có khả năng chụp các hệ thống mạch máu trong cơ thể mà không cần tiêm thuốc đối quang từ giúp giảm thiểu các tác dụng phụ cho người bệnh. Phòng khám có quy mô 1.200 m2, đầy đủ các chuyên khoa: Nội tổng quát, Tai – Mũi – Họng, Tim mạch, Xương khớp, Da liễu, Sản Phụ khoa, Nhi khoa, các dịch vụ Chẩn đoán hình ảnh, Xét nghiệm và hệ thống nhà thuốc.Phòng khám có sự kết nối thông tin, chia sẻ dữ liệu và nhận được sự phối hợp hỗ trợ của Bệnh viện Đa khoa Quốc tế Vinmec. Với sự hỗ trợ từ Trung tâm Công nghệ Gen của Bệnh viện, Phòng khám có khả năng thực hiện sớm các sàng lọc chuyên sâu, chẩn đoán ung thư sơm, xác định chính xác các đột biến Gen gây ung thư để điều trị bằng loại thuốc đặc biệt.`,
        FanpageFB: 'https://www.facebook.com/Vinmec/',
        createdAt: Sequelize.literal('NOW()'),
        updatedAt: Sequelize.literal('NOW()')
      },
      {
        Ten: 'Phòng khám Đa khoa Đại Đông',
        SoDienThoai: '02837194555',
        // DiaChi: '461 Cộng Hòa, 15, Tân Bình, Hồ Chí Minh',
        Avatar: '/images/phongKham/6/avatar.jpg',
        Gallery: '/images/phongKham/6/gallery',
        Thumbnail: '/images/phongKham/6/thumbnail.jpg',
        DiemTrungBinh: 0,
        // NguoiQuanLyId: null,
        GioiThieu: `Phòng Khám Đa Khoa Đại Đông tọa lạc tại địa chỉ 461, Cộng Hòa, Phường 15, quận Tân Bình là một trong những địa chỉ khám chữa bệnh hàng đầu tại trung tâm thành phố Hồ Chí Minh. Chúng tôi là đơn vị y tế được xây dựng quy mô, bài bản, đảm bảo đầy đủ các điều kiện về cơ sở vật chất cũng như nhân sự phục vụ nhu cầu khám chữa của bệnh nhân. Góp một phần nhỏ làm nên hạnh phúc cho hàng nghìn gia đình người bệnh.`,
        FanpageFB: 'https://www.facebook.com/PhongKhamDaiDong/',
        createdAt: Sequelize.literal('NOW()'),
        updatedAt: Sequelize.literal('NOW()')
      }
    ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('PhongKhams', null, {});
  }
};
