'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Users', [
      {
        Username: 'nguyendacnghia',
        Password: '$2a$10$Rp9EBBdKxmNRHkWehhyCHOWYOPsmJEqBnHEIf4jRqCK236iV7Ew2y',
        HoTen: 'Nguyễn Đắc Nghĩa',
        SoDienThoai: '0338123321',
        Email: 'nguyendacnghia@gmail.com',
        GioiTinh: 1,
        NgaySinh: '03/20/1970',
        LoaiUser: 2,
        BangCap: 'Bác sĩ chuyên khoa II, tiến sĩ',
        Avatar: '/images/user/1.jpeg',
        TrangThai: true,
        createdAt: Sequelize.literal('NOW()'),
        updatedAt: Sequelize.literal('NOW()'),
        ChuyenKhoaId: 1,
        ThongTinChiTiet: `## Giới Thiệu:
* Trưởng khoa bệnh viện chợ rẫy
* Bác sĩ hỗ trợ chuyên môn - bệnh viện Quốc tế
* Bác sĩ tại phòng khám Alan Smith.
## Kinh Nghiệm:
* 30 năm nghiên cứu thần kinh học
* 10 năm nghiên cứu các giải pháp cải thiện não bộ
## Giải thưởng
* Bằng khen của bộ Y tế
* Danh hiệu thầy thuốc ưu tú
* Bằng khen về thành tích nghiên cứu khoa học`,
        token: 'bc55415e78e4767f'
      },
      {
        Username: 'nguyenthihoan',
        Password: '$2a$10$Rp9EBBdKxmNRHkWehhyCHOWYOPsmJEqBnHEIf4jRqCK236iV7Ew2y',
        HoTen: 'Nguyễn Thị Hoàn',
        SoDienThoai: '0909033330',
        Email: 'nguyenthihoan@gmail.com',
        GioiTinh: 2,
        NgaySinh: '05/04/1968',
        LoaiUser: 2,
        BangCap: 'Bác sĩ, phó giáo sư, tiến sĩ',
        Avatar: '/images/user/2.jpeg',
        TrangThai: true,
        createdAt: Sequelize.literal('NOW()'),
        updatedAt: Sequelize.literal('NOW()'),
        ChuyenKhoaId: 2,
        ThongTinChiTiet: ``,
        token: '24f612933b3cebd4'
      },
      {
        Username: 'nguyenthanhliem',
        Password: '$2a$10$Rp9EBBdKxmNRHkWehhyCHOWYOPsmJEqBnHEIf4jRqCK236iV7Ew2y',
        HoTen: 'Nguyễn Thanh Liêm',
        SoDienThoai: '0338123321',
        Email: 'nguyenthanhliem@gmail.com',
        GioiTinh: 1,
        NgaySinh: '06/23/1960',
        LoaiUser: 2,
        BangCap: 'Giáo sư, tiến sĩ',
        Avatar: '/images/user/3.jpeg',
        TrangThai: true,
        createdAt: Sequelize.literal('NOW()'),
        updatedAt: Sequelize.literal('NOW()'),
        ChuyenKhoaId: 3,
        ThongTinChiTiet: ``,
        token: 'd6f7c095822d75bd'
      },
      {
        Username: 'Anonymous',
        Password: '$2a$10$Rp9EBBdKxmNRHkWehhyCHOWYOPsmJEqBnHEIf4jRqCK236iV7Ew2y',
        HoTen: 'Nguyễn Văn A',
        SoDienThoai: '011123123123',
        Email: 'nguyenvana@gmail.com',
        GioiTinh: 1,
        NgaySinh: '07/01/2000',
        LoaiUser: 3,
        BangCap: null,
        Avatar: '/images/user/4.png',
        TrangThai: true,
        createdAt: Sequelize.literal('NOW()'),
        updatedAt: Sequelize.literal('NOW()'),
        ChuyenKhoaId: null,
        token: '2e5c69b165bc3e03'
      },
      {
        Username: 'Shiba',
        Password: '$2a$10$Rp9EBBdKxmNRHkWehhyCHOWYOPsmJEqBnHEIf4jRqCK236iV7Ew2y',
        HoTen: 'shi văn ba',
        SoDienThoai: '0445312311',
        Email: 'aloalo1234@gmail.com',
        GioiTinh: 1,
        NgaySinh: '12/21/1999',
        LoaiUser: 3,
        BangCap: null,
        Avatar: '/images/user/5.png',
        TrangThai: true,
        createdAt: Sequelize.literal('NOW()'),
        updatedAt: Sequelize.literal('NOW()'),
        ChuyenKhoaId: null,
        token: '0507c415e68dbbb8'
      },
      {
        Username: 'Pepe',
        Password: '$2a$10$Rp9EBBdKxmNRHkWehhyCHOWYOPsmJEqBnHEIf4jRqCK236iV7Ew2y',
        HoTen: 'Huỳnh Văn Pê',
        SoDienThoai: '0338123321',
        Email: 'huynhvanpe@gmail.com',
        GioiTinh: 1,
        NgaySinh: '01/01/1990',
        LoaiUser: 3,
        BangCap: null,
        Avatar: '/images/user/6.png',
        TrangThai: true,
        createdAt: Sequelize.literal('NOW()'),
        updatedAt: Sequelize.literal('NOW()'),
        ChuyenKhoaId: null,
        token: '92d9be7364354df4'
      },
      {
        Username: 'Qoobee',
        Password: '$2a$10$Rp9EBBdKxmNRHkWehhyCHOWYOPsmJEqBnHEIf4jRqCK236iV7Ew2y',
        HoTen: 'Trần Thanh Trung',
        SoDienThoai: '0338123321',
        Email: 'tranthanhtrun@gmail.com',
        GioiTinh: 1,
        NgaySinh: '08/07/1995',
        LoaiUser: 3,
        BangCap: null,
        Avatar: '/images/user/7.png',
        TrangThai: true,
        createdAt: Sequelize.literal('NOW()'),
        updatedAt: Sequelize.literal('NOW()'),
        ChuyenKhoaId: null,
        token: 'f5999e9be198ffda'
      },
      {
        Username: 'imbatman',
        Password: '$2a$10$Rp9EBBdKxmNRHkWehhyCHOWYOPsmJEqBnHEIf4jRqCK236iV7Ew2y',
        HoTen: 'Bruce Wayne',
        SoDienThoai: '0338123321',
        Email: 'thebatman123@gmail.com',
        GioiTinh: 1,
        NgaySinh: '04/03/1975',
        LoaiUser: 3,
        BangCap: null,
        Avatar: '/images/user/8.png',
        TrangThai: true,
        createdAt: Sequelize.literal('NOW()'),
        updatedAt: Sequelize.literal('NOW()'),
        ChuyenKhoaId: null,
        token: '65bf68a8365ad8bd'
      },
      {
        Username: 'admin',
        Password: '$2a$10$Rp9EBBdKxmNRHkWehhyCHOWYOPsmJEqBnHEIf4jRqCK236iV7Ew2y',
        HoTen: 'Bruce Wayne',
        SoDienThoai: '0338123321',
        Email: 'admin@ad.min',
        GioiTinh: 1,
        NgaySinh: '04/03/1975',
        LoaiUser: 1,
        BangCap: null,
        Avatar: '/images/user/8.png',
        TrangThai: true,
        createdAt: Sequelize.literal('NOW()'),
        updatedAt: Sequelize.literal('NOW()'),
        ChuyenKhoaId: null,
        token: '6114ae2471d7e0be'
      },
      {
        Username: 'lam',
        Password: '$2a$10$Rp9EBBdKxmNRHkWehhyCHOWYOPsmJEqBnHEIf4jRqCK236iV7Ew2y',
        HoTen: 'Lam Trinh',
        SoDienThoai: '0123456789',
        Email: 'lam@trinh.hoang',
        GioiTinh: 1,
        NgaySinh: '04/21/1998',
        LoaiUser: 4,
        BangCap: null,
        Avatar: '/images/user/8.png',
        TrangThai: true,
        createdAt: Sequelize.literal('NOW()'),
        updatedAt: Sequelize.literal('NOW()'),
        ChuyenKhoaId: null,
        PhongKhamId: 4,
        token: 'ee5bbf9fa31128b3'
      }
    ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Users', null, {});
  }
};
