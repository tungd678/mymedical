'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('QuangCaos', [
      {
        Ten: 'sale1',
        HinhAnh: '/images/1.jpg',
        TrangThai: true,
        ThoiGianBatDau: new Date(),
        ThoiGianKetThuc: `1/1/2020`,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        Ten: 'sale2',
        HinhAnh: '/images/2.jpg',
        TrangThai: true,
        ThoiGianBatDau: new Date(),
        ThoiGianKetThuc: `1/1/2020`,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        Ten: 'sale3',
        HinhAnh: '/images/3.jpg',
        TrangThai: true,
        ThoiGianBatDau: new Date(),
        ThoiGianKetThuc: `1/1/2020`,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        Ten: 'sale4',
        HinhAnh: '/images/4.jpg',
        TrangThai: true,
        ThoiGianBatDau: new Date(),
        ThoiGianKetThuc: `1/1/2020`,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        Ten: 'sale5',
        HinhAnh: '/images/5.jpg',
        TrangThai: true,
        ThoiGianBatDau: new Date(),
        ThoiGianKetThuc: `1/1/2020`,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        Ten: 'sale6',
        HinhAnh: '/images/6.png',
        TrangThai: true,
        ThoiGianBatDau: new Date(),
        ThoiGianKetThuc: `1/1/2020`,
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('QuangCaos', null, {});
  }
};
