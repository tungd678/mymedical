'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('GomCacKhoas', [
      {
        PhongKhamId: 4,
        ChuyenKhoaId: 3,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        PhongKhamId: 4,
        ChuyenKhoaId: 4,
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('GomCacKhoas', null, {});
  }
};
