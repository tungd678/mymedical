"use strict";

const bcrypt = require("bcryptjs");

const SALT_WORK_FACTOR = 10;

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    Username: {
      type: DataTypes.STRING,
      unique: true,
      // validate: {
      //   allowNull: false,
      //   notEmpty: true
      // }
    },
    Password: {
      type: DataTypes.STRING,
      // validate: {
      //   allowNull: false,
      //   notEmpty: true
      // }
    },
    HoTen: DataTypes.STRING,
    SoDienThoai: DataTypes.STRING,
    Email: DataTypes.STRING,
    GioiTinh: DataTypes.INTEGER,
    NgaySinh: DataTypes.DATE,
    LoaiUser: DataTypes.INTEGER,
    BangCap: DataTypes.TEXT,
    Avatar: DataTypes.STRING,
    ThongTinChiTiet: DataTypes.TEXT,
    TrangThai: DataTypes.BOOLEAN,
    token: DataTypes.STRING,
    FacebookId: DataTypes.STRING
  },
    {
      classMethods: {
        validPassword(password, passwd, done, user) {
          bcrypt.compare(password, passwd, (err, isMatch) => {
            if (err) console.log(err);
            if (isMatch) {
              return done(null, user);
            } else {
              return done(null, false);
            }
          });
        }
      }
    },
    {
      dialect: 'postgres'
    });
  User.associate = function (models) {
    // associations can be defined here
    User.belongsTo(models.ChuyenKhoa);
    User.hasMany(models.Feedback);
    User.hasOne(models.TheTinDung);
    User.hasMany(models.ThanhToan);
    User.belongsToMany(models.PhongKham, {
      as: 'PhongKham',
      through: models.LamViecTai,
      foreignKey: 'BacSiId'
    });
    User.belongsTo(models.PhongKham, {
      as: 'QuanLy',
      foreignKey: 'PhongKhamId'
    });
    // User.hasOne(models.PhongKham);
  };
  // User.beforeCreate((user, fn) => {
  //   const salt = bcrypt.genSalt(SALT_WORK_FACTOR, (err, salt) => {
  //     return salt;
  //   });
  //   bcrypt.hash(user.Password, salt, null, (err, hash) => {
  //     if (err) return next(err);
  //     user.Password = hash;
  //     return fn(null, user);
  //   });
  // });
  // User.beforeCreate((user, options) => {
  //   const salt = bcrypt.genSaltSync(SALT_WORK_FACTOR);
  //   User.Password = bcrypt.hashSync(user.Password, salt);
  // });
  return User;
};