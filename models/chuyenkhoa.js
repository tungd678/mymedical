'use strict';
module.exports = (sequelize, DataTypes) => {
  const ChuyenKhoa = sequelize.define('ChuyenKhoa', {
    Ten: DataTypes.STRING,
    MoTa: DataTypes.TEXT
  }, {});
  ChuyenKhoa.associate = function(models) {
    // associations can be defined here
    ChuyenKhoa.belongsToMany(models.PhongKham, {
      as: 'ChuyenKhoa',
      through: models.GomCacKhoa,
      foreignKey: 'PhongKhamId'
    });
    ChuyenKhoa.hasMany(models.GomCacKhoa);
    ChuyenKhoa.hasMany(models.User);
  };
  return ChuyenKhoa;
};