'use strict';
module.exports = (sequelize, DataTypes) => {
  const QuanHuyen = sequelize.define('QuanHuyen', {
    type: DataTypes.STRING,
    name: DataTypes.STRING,
    slug: DataTypes.STRING,
    name_with_type: DataTypes.STRING,
    path: DataTypes.STRING,
    path_with_type: DataTypes.STRING
  }, {});
  QuanHuyen.associate = function(models) {
    // associations can be defined here
    QuanHuyen.belongsTo(models.TinhThanhPho);
    QuanHuyen.hasMany(models.ChiTietDiaChi);
  };
  return QuanHuyen;
};