'use strict';
module.exports = (sequelize, DataTypes) => {
  const GomCacKhoa = sequelize.define('GomCacKhoa', {}, {});
  GomCacKhoa.associate = function(models) {
    // associations can be defined here
    GomCacKhoa.belongsTo(models.PhongKham);
    GomCacKhoa.belongsTo(models.ChuyenKhoa);
    // GomCacKhoa.hasOne(models.PhongKham);
    // GomCacKhoa.hasOne(models.ChuyenKhoa);
  };
  return GomCacKhoa;
};