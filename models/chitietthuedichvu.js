'use strict';
module.exports = (sequelize, DataTypes) => {
  const ChiTietThueDichVu = sequelize.define('ChiTietThueDichVu', {
    ThoiGianBatDau: DataTypes.DATE,
    ThoiGianKetThuc: DataTypes.DATE
  }, {});
  ChiTietThueDichVu.associate = function(models) {
    // associations can be defined here
    ChiTietThueDichVu.belongsTo(models.GoiDichVu);
    ChiTietThueDichVu.belongsTo(models.PhongKham);
    ChiTietThueDichVu.hasOne(models.ThanhToan);
  };
  return ChiTietThueDichVu;
};