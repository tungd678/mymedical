'use strict';
module.exports = (sequelize, DataTypes) => {
  const TheTinDung = sequelize.define('TheTinDung', {
    HoTen: DataTypes.STRING,
    SoHieu: DataTypes.STRING,
    NgayHetHan: DataTypes.DATE,
    CSV: DataTypes.STRING
  }, {});
  TheTinDung.associate = function(models) {
    // associations can be defined here
    TheTinDung.hasMany(models.ThanhToan);
    TheTinDung.belongsTo(models.User);
  };
  return TheTinDung;
};