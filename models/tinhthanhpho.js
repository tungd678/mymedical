'use strict';
module.exports = (sequelize, DataTypes) => {
  const TinhThanhPho = sequelize.define('TinhThanhPho', {
    type: DataTypes.STRING,
    name: DataTypes.STRING,
    slug: DataTypes.STRING,
    name_with_type: DataTypes.STRING
  }, {});
  TinhThanhPho.associate = function(models) {
    // associations can be defined here
    TinhThanhPho.hasMany(models.QuanHuyen);
  };
  return TinhThanhPho;
};