'use strict';
module.exports = (sequelize, DataTypes) => {
  const KhuyenMai = sequelize.define('KhuyenMai', {
    Ten: DataTypes.STRING,
    TrangThai: DataTypes.BOOLEAN,
    ThoiGianBatDau: DataTypes.DATE,
    ThoiGianKetThuc: DataTypes.DATE,
    MucGiam: DataTypes.NUMERIC,
    Code: DataTypes.STRING
  }, {});
  KhuyenMai.associate = function(models) {
    // associations can be defined here
    KhuyenMai.hasMany(models.QuangCao);
    KhuyenMai.hasMany(models.SuDungKhuyenMai);
  };
  return KhuyenMai;
};