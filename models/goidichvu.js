'use strict';
module.exports = (sequelize, DataTypes) => {
  const GoiDichVu = sequelize.define('GoiDichVu', {
    Loai: DataTypes.INTEGER,
    Gia: DataTypes.NUMERIC,
    Mota: DataTypes.TEXT
  }, {});
  GoiDichVu.associate = function(models) {
    // associations can be defined here
    GoiDichVu.hasMany(models.ChiTietThueDichVu);
  };
  return GoiDichVu;
};