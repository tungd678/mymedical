'use strict';
module.exports = (sequelize, DataTypes) => {
  const LamViecTai = sequelize.define('LamViecTai', {
    TrangThai: DataTypes.BOOLEAN
  }, {});
  LamViecTai.associate = function(models) {
    // associations can be defined here
    LamViecTai.belongsTo(models.User, { foreignKey: 'BacSiId' });
    LamViecTai.belongsTo(models.PhongKham)
  };
  return LamViecTai;
};