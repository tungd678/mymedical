'use strict';
module.exports = (sequelize, DataTypes) => {
  const QuangCao = sequelize.define('QuangCao', {
    Ten: DataTypes.STRING,
    HinhAnh: DataTypes.STRING,
    TrangThai: DataTypes.BOOLEAN,
    ThoiGianBatDau: DataTypes.DATE,
    ThoiGianKetThuc: DataTypes.DATE,
    Loai: DataTypes.INTEGER //1-Quang cao cho khuyen mai,  2-Quang cao cho phong kham
  }, {});
  QuangCao.associate = function(models) {
    // associations can be defined here
    QuangCao.belongsTo(models.PhongKham);
    QuangCao.belongsTo(models.KhuyenMai);
  };
  return QuangCao;
};