'use strict';
module.exports = (sequelize, DataTypes) => {
  const Feedback = sequelize.define('Feedback', {
    NoiDung: DataTypes.STRING,
    ThoiGian: DataTypes.DATE,
    SoDiem: DataTypes.INTEGER
  }, {});
  Feedback.associate = function(models) {
    // associations can be defined here
    Feedback.belongsTo(models.User);
    Feedback.belongsTo(models.PhongKham);
  };
  return Feedback;
};