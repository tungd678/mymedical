'use strict';
module.exports = (sequelize, DataTypes) => {
  const ThanhToan = sequelize.define('ThanhToan', {
    NgayThanhToan: DataTypes.DATE,
    TongGiaTri: DataTypes.NUMERIC
  }, {});
  ThanhToan.associate = function(models) {
    // associations can be defined here
    ThanhToan.belongsTo(models.ChiTietThueDichVu);
    ThanhToan.belongsTo(models.TheTinDung);
    ThanhToan.belongsTo(models.User);
  };
  return ThanhToan;
};