'use strict';
module.exports = (sequelize, DataTypes) => {
  const LichHen = sequelize.define('LichHen', {
    // NgayKham: DataTypes.DATE,
    // GioKham: DataTypes.DATE,
    ThoiGianKham: DataTypes.DATE,
    HoTen: DataTypes.STRING,
    Email: DataTypes.STRING,
    SoDienThoai: DataTypes.STRING,
    GhiChu: DataTypes.TEXT,
    BacSi: DataTypes.STRING,
    KhoaKhamBenh: DataTypes.STRING,
    TrangThai: DataTypes.INTEGER // 1-chua xu ly, 2-da chap nhan, 3-da huy
  }, {});
  LichHen.associate = function (models) {
    // associations can be defined here
    LichHen.belongsTo(models.User);
    LichHen.belongsTo(models.PhongKham);
  };
  return LichHen;
};