'use strict';
module.exports = (sequelize, DataTypes) => {
    const ChiTietDiaChi = sequelize.define('ChiTietDiaChi', {
        PhongKhamId: {
            type: DataTypes.INTEGER,
            primaryKey: true
        },
        TinhThanhPhoId: DataTypes.INTEGER,
        QuanHuyenId: DataTypes.INTEGER,
        Duong: DataTypes.STRING
    }, {});
    ChiTietDiaChi.associate = function (models) {
        ChiTietDiaChi.belongsTo(models.PhongKham);
        ChiTietDiaChi.belongsTo(models.QuanHuyen);
    };
    return ChiTietDiaChi;
}