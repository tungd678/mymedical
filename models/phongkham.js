'use strict';
module.exports = (sequelize, DataTypes) => {
	const PhongKham = sequelize.define(
		'PhongKham',
		{
			Ten: DataTypes.STRING,
			SoDienThoai: DataTypes.STRING,
			GioiThieu: DataTypes.TEXT,
			Avatar: DataTypes.STRING,
			Thumbnail: DataTypes.STRING,
			Gallery: DataTypes.STRING,
			DiemTrungBinh: DataTypes.NUMERIC,
			FanpageFB: DataTypes.STRING
		},
		{}
	);
	PhongKham.associate = function (models) {
		// associations can be defined here
		PhongKham.hasMany(models.Feedback);
		PhongKham.hasMany(models.ChiTietThueDichVu);
		PhongKham.hasMany(models.QuangCao);
		PhongKham.hasOne(models.ChiTietDiaChi);
		PhongKham.hasMany(models.SuDungKhuyenMai);
		PhongKham.belongsToMany(models.User, {
			as: 'BacSi',
			through: models.LamViecTai,
			foreignKey: 'PhongKhamId'
		});
		PhongKham.belongsToMany(models.ChuyenKhoa, {
			as: 'ChuyenKhoa',
			through: models.GomCacKhoa,
			foreignKey: 'ChuyenKhoaId'
		});
		// PhongKham.belongsTo(models.User, {
		// 	as: 'NguoiQuanLy',
		// 	foreignKey: 'NguoiQuanLyId'
		// });
		// PhongKham.hasOne(models.User);
	};
	return PhongKham;
};
