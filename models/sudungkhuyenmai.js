'use strict';
module.exports = (sequelize, DataTypes) => {
  const SuDungKhuyenMai = sequelize.define('SuDungKhuyenMai', {
    NgaySuDung: DataTypes.DATE,
    SoTienDuocGiam: DataTypes.NUMERIC
  }, {});
  SuDungKhuyenMai.associate = function(models) {
    // associations can be defined here
    SuDungKhuyenMai.belongsTo(models.User);
    SuDungKhuyenMai.belongsTo(models.PhongKham);
    SuDungKhuyenMai.belongsTo(models.KhuyenMai);
  };
  return SuDungKhuyenMai;
};