const localStrategy = require('passport-local').Strategy;
const FacebookStrategy = require('passport-facebook').Strategy;
const bcrypt = require('bcryptjs');
const configAuth = require('./auth');

const { User } = require('../models');

module.exports = passport => {
    passport.use(new localStrategy(
        function (Username, Password, done) {
            User.findOne({
                where: { Username }
            })
                .then(user => {
                    if (!user) {
                        return done(null, false, { message: 'Sai tên đăng nhập!' });
                    }
                    if (!bcrypt.compareSync(Password, user.Password)) {
                        return done(null, false, { message: 'Sai mật khẩu!' });
                    }
                    if (!user.TrangThai) {
                        return done(null, false, { message: 'Tài khoản chưa được xác thực' });
                    }
                    return done(null, user);
                })
                .catch(err => {
                    return done(err);
                });
        }
    ));

    passport.use(new FacebookStrategy({
        clientID: configAuth.facebookAuth.clientID,
        clientSecret: configAuth.facebookAuth.clientSecret,
        callbackURL: configAuth.facebookAuth.callbackURL,
        profileFields: ['id', 'emails', 'displayName', 'photos']
    }, (token, refreshToken, profile, done) => {
        User
            .findOne({where:{FacebookId: profile.id}})
            .then(user => {
                if (user) {
                    done(null, user);
                } else {
                    // console.log(profile);
                    return User.create({
                        FacebookId: profile.id,
                        token,
                        HoTen: profile.displayName,
                        Email: profile.emails[0].value,
                        Avatar: profile.photos[0].value,
                        Username: profile.username
                    });
                }
            })
            .then(newUser => {
                if (newUser) {
                    return done(null, newUser);
                }
            })
            .catch(err => {
                return done(err);
            });
    }));

    passport.serializeUser(function (user, done) {
        done(null, user.id);
    });
    passport.deserializeUser(function (id, done) {
        User.findOne({ where: id })
            .then(user => done(null, user))
            .catch(err => done(err, null));
    });
};