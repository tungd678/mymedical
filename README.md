# PTUDW-16CLC2-Nhom08
## Thành viên
* 1653109 - Trịnh Hoàng Lâm
* 1653096 - Nguyễn Văn Trọng
* 1653101 - Đặng Sơn Tùng 
## Chủ đề 
* Trang web cung cấp một nơi để các phòng khám và bệnh nhân giao tiếp với nhau.
* Phòng khám đăng thông tin phòng khám.
* Bệnh nhân tìm kiếm và đặt hẹn
## Protype: phần của bệnh nhân
* [link drive](https://drive.google.com/open?id=1Ed2GbNgzReANmPw1GIP7RalH5Zgkl_ff)
* [link dropbox](https://www.dropbox.com/s/xfsrlo05zf8wkw2/MyMedical.xd?dl=0)
* [link xd review](https://xd.adobe.com/view/e0dc6f67-7e06-430f-9bd6-9990b162f11c-b58f/)
## Prototype: phần của admin
* [link drive](https://drive.google.com/file/d/1kcR13fMuQ2ESNXJy3ov7aAsZpOyRyKJ1/view?usp=sharing)
## Tài liệu:
* [link drive](https://drive.google.com/open?id=1Bqw745QpyzZSi1r0kWEvOKtZ9FQ8_yg4&fbclid=IwAR3TV_TCgN2PtJ6Dvhwz0o7ZVFmdxI4PhfOWYxCuXaL2tRrXASiryhrSdvI)
## Hướng dẫn cài đặt
1. Cài đặt nodejs từ [đây](https://nodejs.org/en/)
2. Mở ternimnal gõ
```
$ npm install
$ node app
```
3. Truy cập đến đường dẫn http://localhost:8080/
## Tạo CSDL postgreSQL
1. Tạo CSDL tên **MyMedical**
2. Truy cập route /sync để tạo bảng
3. Vào terminal
```
sequelize db:migrate
sequelize db:seed:all
```
## Đăng nhập với tài khoản admin:
* Username: admin
* Password: 123