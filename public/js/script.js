$('#carouselExample').on('slide.bs.carousel', function (e) {
    var $e = $(e.relatedTarget);
    var idx = $e.index();
    var itemsPerSlide = 4;
    var totalItems = $('.carousel-item').length;

    if (idx >= totalItems - (itemsPerSlide - 1)) {
        var it = itemsPerSlide - (totalItems - idx);
        for (var i = 0; i < it; i++) {
            // append slides to end
            if (e.direction == "left") {
                $('.carousel-item').eq(i).appendTo('.carousel-inner');
            }
            else {
                $('.carousel-item').eq(0).appendTo('.carousel-inner');
            }
        }
    }
});


$('#carouselExample').carousel({
    interval: 5000
});

$(document).ready(function () {
    /* show lightbox when clicking a thumbnail */
    $('a.thumb').click(function (event) {
        event.preventDefault();
        var content = $('.modal-body');
        content.empty();
        var title = $(this).attr("title");
        $('.modal-title').html(title);
        content.html($(this).html());
        $(".modal-profile").modal({ show: true });
    });

    $(document).on("click", '[data-toggle="lightbox"]', function (event) {
        event.preventDefault();
        $(this).ekkoLightbox();
    });

});

const setAutocompleteForNavSearch = () => {
    const _baseUrl = document.location.origin;
    const options = {
        url: _baseUrl + '/api/danh-sach-phong-kham.json',
        getValue: 'Ten',
        list: {
            match: {
                enabled: true
            }
        },
        theme: 'square'
    }

    $('#search-phong-kham').easyAutocomplete(options);
};

setAutocompleteForNavSearch();

const setAutocompleteForStaff = () => {
    const _baseUrl = document.location.origin;
    const id = $('#PhongKhamId').val();
    const options = {
        url: `${_baseUrl}/api/danh-sach-bac-si/${id}.json`,
        getValue: 'HoTen',
        template: {
            type: 'iconLeft',
            fields: {
                iconSrc: 'Avatar'
            }
        },
        list: {
            onSelectItemEvent: function () {
                const value = $("#staff").getSelectedItemData();
                $('#idBacSi').html(value.id);
                $('#avatarBacSi').html(value.Avatar);
                $('#hotenBacSi').html(value.HoTen);
            }
        }
    }

    $('#staff').easyAutocomplete(options);
    document.getElementsByClassName("eac-icon-left")[0].style.width = "100%";
};

if (document.getElementById('staff')) {
    setAutocompleteForStaff();
}

(function () {
    const x = document.getElementById("snackbar");
    if (x) {
        x.className = "show";
        setTimeout(function () { x.className = x.className.replace("show", ""); }, 3000);
    }
})();