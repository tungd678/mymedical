let currentTab = document.getElementById('tab-1');

function toggleTab(id) {
    currentTab.style.display = "none";
    currentTab = document.getElementById(`tab-${id}`);
    currentTab.style.display = "block";
}

$(function () {
    $('[data-toggle="tooltip"]').tooltip()
});

document.getElementById('Them').addEventListener('click', (e) => {
    e.preventDefault();
    const id = parseInt($('#idBacSi')[0].innerText);
    const Avatar = $('#avatarBacSi')[0].innerText;
    const HoTen = $('#hotenBacSi')[0].innerText;
    $('#dsBacSi').append(`
            <div class="chip mb-2">
                <input type="hidden" name="id" value="${id}">
                <img src="${Avatar}" alt="Person" width="96" height="96">
                ${HoTen}
                <span class="closebtn" onclick="this.parentElement.style.display='none'">&times;</span>
            </div>
        `);
});

document.getElementById('Luu').addEventListener('click', (e) => {
    e.preventDefault();

    let _baseUrl = document.location.origin;
    let dsNhanVien = [];


    $.each($('.chip'), (i, data) => {
        if (data.style.display == "none") return true;
        dsNhanVien.push(parseInt($('.chip input')[i].value));
    });

    $.ajax({
        type: "POST",
        url: `${_baseUrl}/phong-kham/${$('#PhongKhamId').val()}/nhan-vien?_method=PUT`,
        data: JSON.stringify(dsNhanVien),
        dataType: "json",
        contentType: 'application/json',
        success: function (data) {
            console.log(data);
            location.reload();
        },
        complete: function (data) {
            location.reload();
        }
    });
});

// const readIMG = (input) => {
//     if (input.files && input.files[0]) {
//         let reader = new FileReader();

//         reader.onload = (e) => {
//             $("#imgPreview").attr('src', e.target.result);
//         }

//         reader.readAsDataURL(input.files[0]);
//     }
// }

// $("#img").change(function () {
//     readIMG(this);
// });

Dropzone.options.dropzone = {
    acceptedFiles: "image/*",
    maxFiles: 5,
    // init: function () {
    //     this.on("queuecomplete", function (file) {
    //         $.get('/phong-kham/gallery', (data) => {
    //             if (data == null) {
    //                 return;
    //             }

    //             let images_html_preview = '';
    //             let images_html_input = '';

    //             images = [];

    //             $.each(data, function (key, value) {
    //                 let data_preview = generateHTMLImgPreview(value);
    //                 let data_input = generateHiddenInputImg(value);

    //                 images.push(value);
    //                 images_html_preview += data_preview;
    //                 images_html_input += data_input;
    //             });

    //             images_html_input += generateHiddenInputImg('');
    //             $('#gallery')[0].innerHTML = images_html_preview;
    //             $('#images')[0].innerHTML = images_html_input;
    //         });
    //     });
}
