var password = document.getElementById("password"),
confirm_password = document.getElementById("confirm_password");

function validataPassword(){
    
    if(password.value != confirm_password.value){
        confirm_password.setCustomValidity("Passwords don't match");
        // document.getElementById("message").style.color= red;
        // document.getElementById("message").textContent = "Not maching";
    }
    else{
        confirm_password.setCustomValidity('');
        // document.getElementById("message").style.color= green;
        // document.getElementById("message").textContent = "Maching";

    }
}

password.onchange = validataPassword;
confirm_password.onkeyup = validataPassword;

function onClickChangeBtn(){
    document.getElementById("message").style.color = blue;
    document.getElementById("message").innerHTML = "Mật khẩu đã thay đổi thành công";
}